package com.bryan.barrett.countdownforandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.bryan.barrett.countdownforandroid.R;

import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.hardware.SensorManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main extends Activity {
	
	SharedPreferences prefs;
	static int fontSize, screenw, screenh, unitw, unith;
	static Typeface scribble, led;
	boolean sound;
	static boolean loaded;
	Button soundBtn;
	static SoundPool soundPool;
	static int SNDundo, SNDwriting, SNDshuffle, SNDtick, SNDtickFinal, SNDpop, SNDbeep1, SNDbeep2, SNDtimeup, SNDback,
		SNDclick, SNDexp, SNDsuccess, SNDlevelup, SNDwrong;
	AudioManager audio;
	public static Context mainContext;
	public static AssetManager mainAssets;
	public static ArrayList<String> dictionary = new ArrayList<String>();
	public static ArrayList<String> conundrums = new ArrayList<String>();
	
	Button play;
	TextView mainTitle;
	ProgressDialog progressDialog;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        
        new LoadViewTask().execute();
	    
        //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
	    soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
	      public void onLoadComplete(SoundPool soundPool, int sampleId,
		          int status) {
	    	  loaded = true;
	      }
	    });
	    SNDclick = soundPool.load(this,  R.raw.click1, 1);
	    SNDtick = soundPool.load(this, R.raw.tick, 1);
	    SNDundo = soundPool.load(this, R.raw.undo, 1);
	    SNDtickFinal = soundPool.load(this, R.raw.tick_final, 1);
	    SNDwriting = soundPool.load(this, R.raw.writing, 1);
	    SNDshuffle = soundPool.load(this, R.raw.shuffle, 1);
	    SNDpop = soundPool.load(this, R.raw.pop, 1);
	    SNDbeep1 = soundPool.load(this, R.raw.beep1, 1);
	    SNDbeep2 = soundPool.load(this, R.raw.beep2, 1);
	    SNDtimeup = soundPool.load(this, R.raw.timeup, 1);
	    SNDback = soundPool.load(this, R.raw.woosh, 1);
	    SNDsuccess = soundPool.load(this, R.raw.success, 1);
	    SNDlevelup = soundPool.load(this, R.raw.levelup, 1);
	    SNDwrong = soundPool.load(this, R.raw.wrong, 1);
        
	    //needed for the ScoreLayoutOne and ScoreLayoutOne which do not extend Activity
	    mainContext = getApplicationContext();
	    mainAssets = getAssets();
        
        //Screen Metrics
        DisplayMetrics metrics = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(metrics);
	    screenw = metrics.widthPixels; screenh = metrics.heightPixels;
	    unitw = (screenw/160); unith = (screenh/20);
	    
	    //fonts
	    scribble= Typeface.createFromAsset(getAssets(), "fonts/scribble.ttf");
	    led= Typeface.createFromAsset(getAssets(), "fonts/led.ttf");
	    
	    play = (Button)findViewById(R.id.playbutton); play.setTypeface(scribble);
        play.setWidth(screenw/2); play.setHeight(screenh/3); 
    	
    	Button score = (Button)findViewById(R.id.scorebutton); score.setHeight(screenw/7); score.setWidth(screenw/7);
    	Button help = (Button)findViewById(R.id.helpbutton); help.setWidth(screenw/7); help.setHeight(screenw/7);
    	Button font = (Button)findViewById(R.id.fontbutton); font.setHeight(screenw/7); font.setWidth(screenw/7);
    	
    	soundBtn = (Button)findViewById(R.id.mainSound); soundBtn.setHeight(screenw/7); soundBtn.setWidth(screenw/7);
    	if(sound == false){
    		soundBtn.setBackgroundResource(R.drawable.soundoff);
    	}
    	mainTitle = (TextView)findViewById(R.id.mainTitle); mainTitle.setTypeface(scribble); 
    }
    
    public void onResume(){
		super.onResume();
		
		//Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	    sound = prefs.getBoolean("sound", true);
	    fontSize = prefs.getInt("fontSize", 27);
	    
		play.setTextSize(fontSize+8);
		mainTitle.setTextSize(fontSize+13);
	}
    
    public void openPlaySubscreen(View view){
    	Intent intent = new Intent(this, playSubscreen.class);
    	startActivity(intent);
    	if(sound == true){
    		soundPool.play(SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
    
    public void openAdjustFont(View view){
    	Intent intent = new Intent(this, adjustFont.class);
    	startActivity(intent);
    	if(sound == true){
    		soundPool.play(SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
    
    public void openhelp(View view){
    	Intent intent = new Intent(this, help.class);
    	startActivity(intent);
    	if(sound == true){
    		soundPool.play(SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
    
    public void openscore(View view){
    	Intent intent = new Intent(this, ScoreMenu.class);
    	startActivity(intent);
    	if(sound == true){
    		soundPool.play(SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
    
    public void toggleSound(View view){
    	if(sound == true){
    		sound = false;
    		soundBtn.setBackgroundResource(R.drawable.soundoff);
    	}else if(sound == false){
    		sound = true;
    		soundBtn.setBackgroundResource(R.drawable.soundon);
    	}
		prefs.edit().putBoolean("sound", sound).commit();
		if(sound == true){
			soundPool.play(SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
    
    public void onBackPressed() {
    	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish(); 
	            	conundrums.clear(); dictionary.clear(); 
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Are You Sure You Want To Quit?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
    }
    
  //To use the AsyncTask, it must be subclassed  
    private class LoadViewTask extends AsyncTask<Void, Integer, Void>  
    {

    	protected void onPreExecute()  
        {  
    		progressDialog = ProgressDialog.show(Main.this,"Loading...",  
				    "Setting up dictionary, please wait...", false, false);  
        }  
    	
		@Override
		protected Void doInBackground(Void... arg0) {
		
			//Create the dictionary
	        InputStream dict = getResources().openRawResource(R.raw.xdictionary);
			InputStreamReader isr = new InputStreamReader(dict , Charset.forName ("UTF-8")); 
			BufferedReader br = new BufferedReader(isr );
			String line="";
			try {
				while ((line = br.readLine()) != null) {
					dictionary.add(line);
				}
				br.close(); dict.close(); isr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//Create the conundrums
			InputStream dict2 = getResources().openRawResource(R.raw.conuns);
			InputStreamReader isr2 = new InputStreamReader(dict2 , Charset.forName ("UTF-8")); 
			BufferedReader br2 = new BufferedReader(isr2 );
			String line2="";
			try {
				while ((line2 = br2.readLine()) != null) {
					conundrums.add(line2);
				}
				br2.close(); dict2.close(); isr2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}  
		
		protected void onPostExecute(Void result)  
        {  
			progressDialog.dismiss(); 
        }
    	
    }
}
