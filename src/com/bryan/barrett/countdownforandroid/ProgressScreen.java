package com.bryan.barrett.countdownforandroid;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.HighScoresTable;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProgressScreen extends Activity {

	int gameround, points, viewInt;
	String gamemode; 
	HighScoresTable scoresTable;
	ProgressTable progress;
	TextView title, tround, tpoints, tdataset, tanswer, ttarget, tally;
	Button button;
	SoundPool soundPool;
	AudioManager audio;
	SharedPreferences prefs;
	boolean sound;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_progress_screen);
	
	    //Load from previous activity
	    Intent i = getIntent();
	    points = i.getIntExtra("points", 0);
	    gameround = i.getIntExtra("gameround", 0);
	    gamemode = i.getStringExtra("gamemode");
	    viewInt = i.getIntExtra("viewInt", 0);	//only used for viewing previous games via score menu
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //db
	    progress = new ProgressTable(this);
	    scoresTable = new HighScoresTable(this);
	    
	    title = (TextView)findViewById(R.id.progScreenTitle); title.setTypeface(Main.scribble); title.setTextSize(Main.fontSize+8);
	    
	    tround = (TextView)findViewById(R.id.progScreenTableRound); 
	    tpoints = (TextView)findViewById(R.id.progScreenTablePoints); 
	    tdataset =(TextView)findViewById(R.id.progScreenTableDataSet); 
	    tanswer = (TextView)findViewById(R.id.progScreenTableAnswer); 
	    ttarget = (TextView)findViewById(R.id.progScreenTableTarget); 
	    tally = (TextView)findViewById(R.id.progScreenTally); tally.setTextSize(Main.fontSize-8);
	    
	    if(gamemode.equals("full")){	//user is playing a full game
	    title.setText("Progress after " + gameround + " rounds: ");
	    tally.setText("Points: " + points);
	    
		tround.setText("\n\n" + progress.getAllRounds()); 
		tpoints.setText("Pts:\n\n" + progress.getAllPoints());
		tdataset.setText("Dataset:\n\n" + progress.getAllData());
		tanswer.setText("Your Answer:\n\n" + progress.getAllAnswers()); 
		ttarget.setText("Target:\n\n" + progress.getAllTargets());
	    }else{							//user is viewing a previous game from scores menu
	    	title.setText("Name: " + scoresTable.getColPos(viewInt, "name") + ". Date: " + scoresTable.getColPos(viewInt, "date"));
	    	tally.setText("Points: " + scoresTable.getColPos(viewInt, "points"));
	    	
	    	tround.setText("\n\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n"); 
			tpoints.setText("Pts:\n\n" + scoresTable.getColPos(viewInt, "ppoints")); tpoints.setTextColor(Color.rgb(15, 120, 11));
			tdataset.setText("Dataset:\n\n" + scoresTable.getColPos(viewInt, "pdataset"));
			tanswer.setText("Your Answer:\n\n" + scoresTable.getColPos(viewInt, "panswer")); 
			ttarget.setText("Target:\n\n" + scoresTable.getColPos(viewInt, "ptarget"));
	    }
	    progress.close(); scoresTable.close();
		
		button = (Button)findViewById(R.id.progScreenButton); button.setMinHeight(Main.screenh/5); button.setTextSize(Main.fontSize);
		button.setMaxWidth((Main.screenw/3)*2);
		if(gamemode.equals("score")){
			button.setText(" Back To Scores Menu ");
		}else if(gameround == 15){
			button.setText(" Submit Score ");
		}else if(gameround == 14){
			button.setText(" Final Round: Conundrum ");
		}else{
			if(gameround == 4 || gameround == 9 || gameround == 13){
				button.setText(" Round " + (gameround+1) + ": Numbers Round ");
			}else{
				button.setText(" Round " + (gameround+1) + ": Letters Round ");
			}
		}
	}
	
	public void progScreenNext(View view){
		if(sound == true){
	    	soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
		if(gamemode.equals("score")){
			finish();
		}else if(gameround == 15){
			Intent i = new Intent(this, finalScoreScreen.class);
			i.putExtra("points", points);
			i.putExtra("gamemode", gamemode);
			startActivity(i);
			overridePendingTransition(R.anim.fadein, R.anim.fadeout);
			finish();
		}else if(gameround == 14){
			Intent i = new Intent(this, conundrum.class);
			i.putExtra("points", points);
			i.putExtra("gameround", gameround);
			i.putExtra("gamemode", gamemode);
			startActivity(i);
			overridePendingTransition(R.anim.fadein, R.anim.fadeout);
			finish();
		}else if(gameround == 4 || gameround == 9 || gameround == 13 ){
			Intent i = new Intent(this, numbersSelect.class);
			i.putExtra("points", points);
			i.putExtra("gameround", gameround);
			i.putExtra("gamemode", gamemode);
			startActivity(i);
			overridePendingTransition(R.anim.fadein, R.anim.fadeout);
			finish();
		}else{
			Intent i = new Intent(this, lettersSelect.class);
			i.putExtra("points", points);
			i.putExtra("gameround", gameround);
			i.putExtra("gamemode", gamemode);
			startActivity(i);
			overridePendingTransition(R.anim.fadein, R.anim.fadeout);
			finish();
		}
	}

}
