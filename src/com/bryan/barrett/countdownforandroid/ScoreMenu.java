package com.bryan.barrett.countdownforandroid;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.HighScoresTable;
import com.bryan.barrett.countdownforandroid.viewpager.ScoreLayoutOne;
import com.bryan.barrett.countdownforandroid.viewpager.ScoreLayoutTwo;
import com.bryan.barrett.countdownforandroid.viewpager.ScoreViewPagerAdapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;

public class ScoreMenu extends FragmentActivity {
	int cursorPos;
	private ViewPager _mViewPager;
	private ScoreViewPagerAdapter _adapter;
	
	HighScoresTable scoresTable;
	
	AudioManager audio;
	SharedPreferences prefs;
	boolean sound;
	SoundPool soundPool;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_main);
        
        //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
        cursorPos = 0;
        scoresTable = new HighScoresTable(this);
        ScoreLayoutOne.setTable1(scoresTable.getAllPoints("full"), scoresTable.getAllNames("full"), scoresTable.getAllTimes("full"));
        ScoreLayoutTwo.setTable2(scoresTable.getAllPoints("quick"), scoresTable.getAllNames("quick"), scoresTable.getAllTimes("quick"));
        setUpView();
        setTab();
    }
    
    private void setUpView(){    	
   	 _mViewPager = (ViewPager) findViewById(R.id.viewPager);
     _adapter = new ScoreViewPagerAdapter(getApplicationContext(),getSupportFragmentManager());
     _mViewPager.setAdapter(_adapter);
	 _mViewPager.setCurrentItem(0);
    }
    
    private void setTab(){
			_mViewPager.setOnPageChangeListener(new OnPageChangeListener(){
			    		
						@Override
						public void onPageScrollStateChanged(int position) {}
						@Override
						public void onPageScrolled(int arg0, float arg1, int arg2) {}
						@Override
						public void onPageSelected(int position) {
							if(sound){
						    	soundPool.play(Main.SNDshuffle, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
						    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
						    }
							switch(position){
							case 0:
								findViewById(R.id.first_tab).setVisibility(View.VISIBLE);
								findViewById(R.id.second_tab).setVisibility(View.INVISIBLE);
								break;
								
							case 1:
								findViewById(R.id.first_tab).setVisibility(View.INVISIBLE);
								findViewById(R.id.second_tab).setVisibility(View.VISIBLE);
								break;
							}
						}
						
					});

    }
    
    public void viewThisScore(View view){
    	if(scoresTable.getAllNames("full").equals("")){
    		return;
    	}else{
    		if(sound){
    	    	soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	    }
    		Intent i = new Intent(this, ProgressScreen.class);
    		i.putExtra("gamemode", "score");
    		i.putExtra("viewInt", cursorPos);
    		startActivity(i);
    	}
    }
    
    public void cursorDown(View view){
    	if(scoresTable.getAllNames("full").equals("")){
    		return;
    	}else{	
    		if(sound){
    	    	soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	    }
    		if(cursorPos == (scoresTable.getCount("full")-1)){
    			return;
    		}else{
    			cursorPos++;
    		}
    		ScoreLayoutOne.moveCursor(cursorPos);  
    	}
    }
    
    public void cursorUp(View view){
    	if(scoresTable.getAllNames("full").equals("")){
    		return;
    	}else{
    		if(sound){
    	    	soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	    }
    		if(cursorPos == 0){
    			return;
    		}else{
    			cursorPos--;
    		}
    		ScoreLayoutOne.moveCursor(cursorPos);    	
    	}
    }
    
    public void onBackPressed() {
	    super.onBackPressed();
	    if(sound){
	    	soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    }
        scoresTable.close();
	}
}