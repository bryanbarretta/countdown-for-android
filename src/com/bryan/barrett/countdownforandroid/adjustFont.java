package com.bryan.barrett.countdownforandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bryan.barrett.countdownforandroid.R;

public class adjustFont extends Activity {

	Button reset;
	TextView letterTile, numberTile, divideTile, clearTile, undoTile, timerTile, fontNum;
	
	int fontSize, newSize;
	SharedPreferences prefs;
	boolean sound;
	MediaPlayer back;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_adjust_font);
	    
	    // Screen Metrics
	    DisplayMetrics metrics = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(metrics);
	    int screenw = metrics.widthPixels; int screenh = metrics.heightPixels;
	    int unitw = (screenw/160); int unith = (screenh/20);	//divide screen into 160 columns x 20 rows...
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    newSize = fontSize = prefs.getInt("fontSize", 27);
	    sound = prefs.getBoolean("sound", true);
	    
	    //sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    back = MediaPlayer.create(getApplicationContext(), R.raw.woosh);
	    
	    reset = (Button)findViewById(R.id.adjustResetBtn); reset.setWidth(screenw/2);
	    
	    fontNum = (TextView)findViewById(R.id.fontSizeNum); fontNum.setText("" + newSize);
	    LinearLayout adjLay = (LinearLayout)findViewById(R.id.adjustLayout); 
	    android.view.ViewGroup.LayoutParams aLparams = adjLay.getLayoutParams(); aLparams.height = unith*4;
	    
	    letterTile = (TextView)findViewById(R.id.adjustLetterTile); letterTile.setTextSize(fontSize);
	    letterTile.setWidth(screenw/9); letterTile.setHeight(unith*4);
	    numberTile = (TextView)findViewById(R.id.adjustNumberTile); numberTile.setTextSize(fontSize);
	    numberTile.setWidth(screenw/6); numberTile.setHeight(unith*5);
	    
	    divideTile = (TextView)findViewById(R.id.adjustDivideTile); divideTile.setTextSize(fontSize+10);
	    divideTile.setWidth(screenw/6); divideTile.setHeight((int) (unith*5.5));
	    
	    LinearLayout clearTil = (LinearLayout)findViewById(R.id.adjustClearTile); 
	    android.view.ViewGroup.LayoutParams cTparams = clearTil.getLayoutParams(); 
	    cTparams.height = screenw/6; cTparams.width = (int) (unith*5.5);
	    clearTile = (TextView)findViewById(R.id.adjustClearTileTXT); clearTile.setTextSize(fontSize-10);
	    
	    LinearLayout undoTil = (LinearLayout)findViewById(R.id.adjustUndoTile); 
	    android.view.ViewGroup.LayoutParams uTparams = undoTil.getLayoutParams(); 
	    uTparams.height = unith*4; uTparams.width = unitw*37;
	    undoTile = (TextView)findViewById(R.id.adjustUndoTileText); undoTile.setTextSize(fontSize-10);
	    
	    timerTile = (TextView)findViewById(R.id.adjustTimerTile); timerTile.setTextSize(fontSize+20);
	    Typeface led = Typeface.createFromAsset(getAssets(), "fonts/led.ttf");
	    timerTile.setTypeface(led); timerTile.setHeight(unith*4);
	}
	
	public void resetPressed(View view){
		newSize = 27;
		fontNum.setText("" + newSize);
		letterTile.setTextSize(newSize); numberTile.setTextSize(newSize); divideTile.setTextSize(newSize+10);
		clearTile.setTextSize(newSize-10); undoTile.setTextSize(newSize-10); timerTile.setTextSize(newSize+20);
	}
	
	public void incPressed(View view){
		if(newSize < 60){
			newSize++;
		}
		fontNum.setText("" + newSize);
		letterTile.setTextSize(newSize); numberTile.setTextSize(newSize); divideTile.setTextSize(newSize+10);
		clearTile.setTextSize(newSize-10); undoTile.setTextSize(newSize-10); timerTile.setTextSize(newSize+20);
	}
	
	public void decPressed(View view){
		if(newSize > 20){
			newSize--;
		}
		fontNum.setText("" + newSize);
		letterTile.setTextSize(newSize); numberTile.setTextSize(newSize); divideTile.setTextSize(newSize+10);
		clearTile.setTextSize(newSize-10); undoTile.setTextSize(newSize-10); timerTile.setTextSize(newSize+20);
	}
	
	public void onBackPressed() {
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	fontSize = newSize;
	            }
	            case DialogInterface.BUTTON_NEGATIVE:            	
	            }
	            prefs.edit().putInt("fontSize", fontSize).commit();
            	finish();
	            if(sound){
        	    	back.start();
        	    }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Font Size is now at " + newSize + ". Keep Changes?").setPositiveButton
	    ("Yes (" + newSize + " sp)", dialogClickListener)
	    .setNegativeButton
	    ("No (" + fontSize + " sp)", dialogClickListener).show();
	}

}
