package com.bryan.barrett.countdownforandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import com.bryan.barrett.countdownforandroid.R;

/**FINDS A BEST WORD FROM 9 LETTERS. TAKES APPROX 10 SECONDS SO ITS DONE IN BACKGROUND DURING ROUND **/
public class bgTask extends AsyncTask<String, Void, ArrayList<String>>{

	private static Context context;
	/*Because this class does not extend Activity, I cannot getResources() unless I call context from 
	 a different class that does extend Activity. I call bgTask.setContext in lettersCreate class*/
    public static void setContext(Context mcontext) {
        if (context == null)
            context = mcontext;	//Now I can getResources in solve method i.e. the dictionary file 
    }
    
	protected void onPreExecute() {
		
	}
	
	protected ArrayList<String> doInBackground(String... letters) {
		String word2solve = letters[0];
		ArrayList<String> bestwords = solve(word2solve);
		return bestwords ;
	}
	
	protected void onProgressUpdate() {
		
	}
	
	protected void onPostExecute(ArrayList<String> bestword) {
		super.onPostExecute(bestword);
		lettersCreate.setBestWord(bestword);
	}
	
	public ArrayList<String> solve (String letters){
		letters = letters.toLowerCase();
		String bestword = "";
		ArrayList<String> bestwords = new ArrayList<String>();
		
		InputStream dict = context.getResources().openRawResource(R.raw.xdictionary);
		InputStreamReader isr = new InputStreamReader(dict , Charset.forName ("UTF-8")); 
		BufferedReader br = new BufferedReader(isr );
        String line = "";
        
        try {
			while ((line = br.readLine()) != null) {				//reset thisWordSize
			   String tempWord = "";
			   //-------------------------------------------------
			   
			   ArrayList<Character> charArray = new ArrayList<Character>();
			   for(int x = 0; x < letters.length(); x++){
				   charArray.add(letters.charAt(x));
			   }
			   
			   for(int i = 0; i < line.length(); i++){				//iterate dictionary word
				   for(int j = 0; j < letters.length(); j++){		//iterate scrambled letters
					   if(line.charAt(i)==charArray.get(j)){	//if theres a match...
						   char c = charArray.get(j);
						   tempWord+=c;
						   charArray.set(j, '0');
						   //System.out.println(line + ": Removed letter " + j + " [" + c + "] so remaining letters are "
						   //		   + charArray + ". Tempword is " + tempWord);
						   break;
					    }		   
				   }
			   }
			  
			   if(tempWord.equals(line)){
				   //a word with the same length as the best word
				   bestwords.add(tempWord);
			   }
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        java.util.Collections.sort(bestwords, new bgTaskComparator()); 
		return bestwords ;		
	}
}
