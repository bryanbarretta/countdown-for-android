package com.bryan.barrett.countdownforandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.StringTokenizer;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class conundrum extends Activity implements SensorListener{

	TextView timertext, l1, l2, l3, l4, l5, l6, l7, l8, l9, startText;
	ArrayList<TextView> letters = new ArrayList<TextView>();
	String conundrum, jumbled, answer = "", gamemode;
	Handler timer = new Handler();
	boolean timerInProgress;
	int counter, letcount, points, bonustimeleft=0;
	Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, begin;
	LinearLayout submit, clear, undo;
	ArrayList<Button> buttons = new ArrayList<Button>();
	ArrayList<Integer> btnIDlist = new ArrayList<Integer>();
	Runnable runnable;
	ProgressTable progress;
	SharedPreferences prefs;
	boolean timerInMotion = false, sound;

	BroadcastReceiver br;
	boolean screenOn = true;
	SoundPool soundPool;
	AudioManager audio;
	
	//Shake / Vibrator
	SensorManager sensorMgr;
	Vibrator v;
	long lastUpdate;
	float last_x = 0, last_y = 0, last_z = 0, x = 0, y = 0, z = 0;
	private static final int SHAKE_THRESHOLD = 800;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_conundrum);
	    timerInProgress = false;

		progress = new ProgressTable(this);
		
		//set up a BROADCAST RECEIVER and register it
	    br = new BroadcastReceiver(){
	    	@Override
	        public void onReceive(Context context, Intent intent) {
	    		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
		            //What to do if screen is turned off
	    			screenOn = false;}
		        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
		        	//What to do if user is present at app
		        	screenOn = true;
		        	onResume();
		        }
	        }
	    };
	    IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
	    filter.addAction(Intent.ACTION_SCREEN_OFF);
	    registerReceiver(br, filter);
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    points = i.getIntExtra("points", 0);
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    counter = 32;
	    letcount = 0;
	    
	    startText = (TextView)findViewById(R.id.actconunSTARTtext); startText.setTypeface(Main.scribble); startText.setTextSize(Main.fontSize-5);
	    timertext = (TextView)findViewById(R.id.actconuntimertext); timertext.setHeight(Main.unith*4); timertext.setTextSize(Main.fontSize+20);
	    btn1 = (Button)findViewById(R.id.actconunbtn1); buttons.add(btn1);
	    btn2 = (Button)findViewById(R.id.actconunbtn2); buttons.add(btn2);
	    btn3 = (Button)findViewById(R.id.actconunbtn3); buttons.add(btn3);
	    btn4 = (Button)findViewById(R.id.actconunbtn4); buttons.add(btn4);
	    btn5 = (Button)findViewById(R.id.actconunbtn5); buttons.add(btn5);
	    btn6 = (Button)findViewById(R.id.actconunbtn6); buttons.add(btn6);
	    btn7 = (Button)findViewById(R.id.actconunbtn7); buttons.add(btn7);
	    btn8 = (Button)findViewById(R.id.actconunbtn8); buttons.add(btn8);
	    btn9 = (Button)findViewById(R.id.actconunbtn9); buttons.add(btn9);
	    for(int a = 0; a < 9; a++){
	    	buttons.get(a).setWidth(Main.screenw/9); buttons.get(a).setHeight(Main.unith*4);	
	    	buttons.get(a).setTextSize(Main.fontSize);
	    }
	    begin = (Button)findViewById(R.id.actconunbegin); begin.setHeight(Main.unith*4);
	    l1 = (TextView)findViewById(R.id.actconunl1); letters.add(l1);
	    l2 = (TextView)findViewById(R.id.actconunl2); letters.add(l2);
	    l3 = (TextView)findViewById(R.id.actconunl3); letters.add(l3);
	    l4 = (TextView)findViewById(R.id.actconunl4); letters.add(l4);
	    l5 = (TextView)findViewById(R.id.actconunl5); letters.add(l5);
	    l6 = (TextView)findViewById(R.id.actconunl6); letters.add(l6);
	    l7 = (TextView)findViewById(R.id.actconunl7); letters.add(l7);
	    l8 = (TextView)findViewById(R.id.actconunl8); letters.add(l8);
	    l9 = (TextView)findViewById(R.id.actconunl9); letters.add(l9);
	    for(int l = 0; l < 9; l++){
	    	letters.get(l).setWidth(Main.screenw/9); letters.get(l).setHeight(Main.unith*4); letters.get(l).setTextSize(Main.fontSize);
	    }
	    //Command buttons
	    
	    clear = (LinearLayout)findViewById(R.id.actconunclear); clear.setEnabled(false); 
	    	android.view.ViewGroup.LayoutParams params = clear.getLayoutParams(); params.height = Main.unith*4;
	    	TextView clrTXT = (TextView)findViewById(R.id.actconunclearTXT); clrTXT.setTextSize(Main.fontSize-10);
	    undo = (LinearLayout)findViewById(R.id.actconunundo); undo.setEnabled(false); //undo.setTextSize(fontSize-10);
	    	TextView undTXT = (TextView)findViewById(R.id.actconunundoTXT); undTXT.setTextSize(Main.fontSize-10);
	    submit = (LinearLayout)findViewById(R.id.actconunsubmit); submit.setEnabled(false); //submit.setTextSize(fontSize-10);
	    	TextView subTXT = (TextView)findViewById(R.id.actconunsubmitTXT); subTXT.setTextSize(Main.fontSize-10);
	    
		runnable = new Runnable(){
	            @Override
	            public void run() {
	                timer.postDelayed(this,1000);
	                if(counter == 32){
	                	if(sound){
	                		soundPool.play(Main.SNDbeep1, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	                	}
	                	getconundrum();
	            	    timertext.setClickable(false);
	                	for(int a = 0; a < 9; a++){
	            	    	//buttons.get(a).setText("" + jumbled.charAt(a));
	            	    	buttons.get(a).setBackgroundResource(R.drawable.smalltileonclick);
	            	    	buttons.get(a).setPadding(1, 1, 1, 1);
	            	    	buttons.get(a).setEnabled(true); 	
	            	    }
	                	clear.setEnabled(true);
	                	undo.setEnabled(true);
	                }else if(counter == 31){
	                	if(sound){
	                		soundPool.play(Main.SNDbeep2, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	                	}
	                	timertext.setText("GO!!");
	                	timertext.setTextColor(Color.GREEN);
	                	timertext.setTypeface(Main.led); 
	                	timertext.setTextSize(Main.fontSize+20);
	                	submit.setEnabled(true);
	                	timerInMotion = true;
	                	for(int a = 0; a < 9; a++){
	            	    	buttons.get(a).setText("" + jumbled.charAt(a));
	            	    }
	                }else if(counter == -1){
	                	timertext.setText("TIME UP");            
	                	btn1.setEnabled(false);btn2.setEnabled(false);btn3.setEnabled(false);
	                	btn4.setEnabled(false);btn5.setEnabled(false);btn6.setEnabled(false);
	                	btn7.setEnabled(false);btn8.setEnabled(false);btn9.setEnabled(false);
	                }
	                else if(counter == -2){
	                	//timeup               	
	                	nextRound(gamemode, letters, answer);	
	                }else{
	                	timertext.setTextColor(Color.RED);
	                	if(sound){
	                		if(counter <= 30 && counter > 5){
	                			soundPool.play(Main.SNDtick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	        							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	                		}else if(counter <= 5 && counter >= 1){
	                			soundPool.play(Main.SNDtickFinal, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	        							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	                		}else if(counter == 0){
	                			soundPool.play(Main.SNDtimeup, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	        							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	                		}
	                	}
	                	if(counter>9){
	                	timertext.setText("00:" + counter);
	                	}else{
	                		timertext.setText("00:0" + counter);	
	                	}
	                }                
	                counter--;
	            }
		    };   
	}

	public void onPause(){
		super.onPause();	/** Stop Sensor, Vibrator and Timer **/
		
		sensorMgr.unregisterListener(this);
		v.cancel();
		
		if(timerInProgress == true){
			timer.removeCallbacks(runnable);
		}
	}
	
	public void onResume(){
		super.onResume();	/** Start Sensor, Vibrator and Timer **/
		if(screenOn == true){
			//Sensor
			sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
			sensorMgr.registerListener(this, SensorManager.SENSOR_ACCELEROMETER, SensorManager.SENSOR_DELAY_GAME);
			//Vibrator
			v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    
			if(timerInProgress == true){
				timer.post(runnable);
			}
		}
	}
	
	private void getconundrum() {		
		Random rand = new Random(); String cl = "";
		//int conundrumsCount = rand.nextInt(Main.conundrums.size());
		cl = Main.conundrums.get(rand.nextInt(Main.conundrums.size()));
		//String cl = "ABCEDSIFG, FKSOKLEKD";
		/*StringTokenizer st = new StringTokenizer(cl, ",");
		conundrum = st.nextToken(); jumbled = st.nextToken().toUpperCase();*/
		
		conundrum = cl.substring(0, 9).toUpperCase(); jumbled = cl.substring(11, 20).toUpperCase();
	}

	public void conunbuttonPressed1(View view){char letter = jumbled.charAt(0);int btnID = 0;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed2(View view){char letter = jumbled.charAt(1);int btnID = 1;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed3(View view){char letter = jumbled.charAt(2);int btnID = 2;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed4(View view){char letter = jumbled.charAt(3);int btnID = 3;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed5(View view){char letter = jumbled.charAt(4);int btnID = 4;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed6(View view){char letter = jumbled.charAt(5);int btnID = 5;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed7(View view){char letter = jumbled.charAt(6);int btnID = 6;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed8(View view){char letter = jumbled.charAt(7);int btnID = 7;btnpressed(btn1, letter, btnID);}
	public void conunbuttonPressed9(View view){char letter = jumbled.charAt(8);int btnID = 8;btnpressed(btn1, letter, btnID);}
	
	private void btnpressed(Button btn, char letter, int btnID) {
		if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
		btnIDlist.add(btnID);
		answer += letter;
		buttons.get(btnID).setBackgroundResource(R.drawable.slotempty);
		buttons.get(btnID).setText("");
		buttons.get(btnID).setEnabled(false);
		letters.get(letcount).setVisibility(View.VISIBLE);
		letters.get(letcount).setPadding(1, 1, 1, 1);
		letters.get(letcount).setText("" + letter);
		letcount++;
	}
	
	public void conunbeginbutton(final View view){
    	timertext.setText("Reveal The Conundrum!");
    	timertext.setTextSize(Main.fontSize+5);
	    startText.setText("");
    	begin.setEnabled(false);
    	begin.setBackgroundResource(R.drawable.redbuttongreyed);
    	for(int a = 0; a < 9; a++){
        	buttons.get(a).setBackgroundResource(R.drawable.smalltile);
        	buttons.get(a).setPadding(1, 1, 1, 1);
        }
		timerInProgress = true;
		timer.post(runnable);
	}
	
	public void conunclear(View view){
		if(sound == true){
			soundPool.play(Main.SNDshuffle, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		for(int i = 0; i<9; i++){
			buttons.get(i).setBackgroundResource(R.drawable.smalltileonclick);
			buttons.get(i).setText("" + jumbled.charAt(i));
			buttons.get(i).setPadding(1, 1, 1, 1);
			buttons.get(i).setEnabled(true);
			letters.get(i).setVisibility(View.INVISIBLE);
			letters.get(i).setText("");
		}
		btnIDlist.clear();
		answer = "";
		letcount = 0;
	}
	
	public void conunundo(View view){
		if(btnIDlist.isEmpty()){
			return;
		}else{
			if(sound){
				if(Main.loaded){
				soundPool.play(Main.SNDundo, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
				}
			}
			letcount--;
			letters.get(letcount).setVisibility(View.INVISIBLE);
			letters.get(letcount).setText("");
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setBackgroundResource(R.drawable.smalltileonclick);
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setText("" + jumbled.charAt(btnIDlist.get(btnIDlist.size()-1)));
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setPadding(1, 1, 1, 1);
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setEnabled(true);
			btnIDlist.remove(btnIDlist.size()-1);
			answer = answer.substring(0, answer.length()-1);
			answer.toString();
		}
	}
	
	public void conunsubmit(View view){
		if(sound){
			soundPool.play(Main.SNDbeep2, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		bonustimeleft = counter+1;
		nextRound(gamemode, letters, answer);
	}
	
	private void nextRound(String gamemode,	ArrayList<TextView> letters, String answer) {

	    progress.close();
		Intent intent = new Intent(this, conundrumPoints.class);
		intent.putExtra("jumbled", jumbled);
		intent.putExtra("conundrum", conundrum);
		intent.putExtra("answer", answer);
		intent.putExtra("gamemode", gamemode);
		intent.putExtra("bonustimeleft", bonustimeleft);
		intent.putExtra("points", points);
    	startActivity(intent);
   	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	onFinish();
    	timer.removeCallbacksAndMessages(null);
	}
	
	public void onBackPressed() {
		//make this an undo button
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	onFinish();
	            	if(sound){
	        	    	soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	        					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            	if(gamemode.equals("full")){
	            		progress.add(0, jumbled, "*forfeited", conundrum);
	            	}
            	    progress.close();
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                return;
	            }
	        }
	    };
	    String question = "";
	    if(gamemode.equals("full")){
	    	question = "Are you sure you want to quit? (You will forfeit the round)";
	    }else{
	    	question = "Return to menu?";
	    }
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(question).setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}

	@Override
	public void onAccuracyChanged(int sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(int sensor, float[] values) {
		if (sensor == SensorManager.SENSOR_ACCELEROMETER) {
		    long curTime = System.currentTimeMillis();
		    // only allow one update every 200ms.
		    if ((curTime - lastUpdate) > 200) {
		      long diffTime = (curTime - lastUpdate);
		      lastUpdate = curTime;

		      x = values[SensorManager.DATA_X];
		      y = values[SensorManager.DATA_Y];
		      z = values[SensorManager.DATA_Z];

		      //x+y+z � last_x � last_y � last_z
		      
		      float speed = Math.abs(x+y+z-last_z-last_y-last_x) / diffTime * 10000;

		      if (speed > SHAKE_THRESHOLD && timerInMotion == true) {	//Device is shook
		        //Vibrate Device
		    	v.vibrate(200);
		    	//Clear 
		    	for(int i = 0; i<9; i++){
					buttons.get(i).setBackgroundResource(R.drawable.smalltileonclick);
					buttons.get(i).setText("" + jumbled.charAt(i));
					buttons.get(i).setPadding(1, 1, 1, 1);
					buttons.get(i).setEnabled(true);
					letters.get(i).setVisibility(View.INVISIBLE);
					letters.get(i).setText("");
				}
				btnIDlist.clear();
				answer = "";
				letcount = 0;
				//Shuffle Letters and update UI
				ArrayList <String> al = new ArrayList <String>();
				for(int nn = 0; nn < 9; nn++){
					al.add("" + jumbled.charAt(nn));
				}
		        Collections.shuffle(al);
		        jumbled = "";
		        for(int nn = 0; nn < 9; nn++){
		        	jumbled+=al.get(nn);
		        	buttons.get(nn).setText(al.get(nn));
		        }
		        if(sound){
					soundPool.play(Main.SNDshuffle, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
				}
		      }
		      last_x = x;
		      last_y = y;
		      last_z = z;
		    }
		  }
	}
	
	public void onFinish(){
		unregisterReceiver(br);
		sensorMgr.unregisterListener(this);
		v.cancel();
		finish();
	}
	
}
