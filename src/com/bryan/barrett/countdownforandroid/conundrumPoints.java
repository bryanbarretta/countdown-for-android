 package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class conundrumPoints extends Activity {

	TextView l1, l2, l3, l4, l5, l6, l7, l8, l9, solution, text, pointstext, pointsinfo, levelView, toNextLevelView;
	Button button1, button2;
	ArrayList<TextView> answertiles = new ArrayList<TextView>();
	ProgressBar progressbar;
	String answer, jumbled, conundrum, gamemode;
	Handler reveal = new Handler(); Handler updatepoints = new Handler();
	int anscount = 0, pointscounter = 0, points, max, bonustimeleft, pointslimit;
	long lvl;
	ProgressTable progress;
	Toast levelUpToast;
	SoundPool soundPool;
	AudioManager audio;
	boolean sound;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_conundrum_points);
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    points = i.getIntExtra("points", 0);
	    pointscounter = points%100;
	    answer = i.getStringExtra("answer");
	    jumbled = i.getStringExtra("jumbled");
	    conundrum = i.getStringExtra("conundrum");
	    bonustimeleft = i.getIntExtra("bonustimeleft", 0);
	    
	    if(!answer.equals(conundrum)){
	    	bonustimeleft = 0;
	    }

	    //Shared Preferences
	    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    lvl = prefs.getLong("userlvl", 1);
	    pointslimit = (int) (lvl*10 + 100);
	    pointscounter = (int) prefs.getLong("pointscounter", 0);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //add to progress database
	    progress = new ProgressTable(this);
	    if(gamemode.equals("full")){
	    	if(answer.equals(conundrum)){
	    		progress.add(100 + (bonustimeleft*10), jumbled.toUpperCase(), answer.toUpperCase(), conundrum.toUpperCase());
	    	}else{
	    		progress.add(0, jumbled.toUpperCase(), answer.toUpperCase(), conundrum.toUpperCase());
	    	}
	    }
	    progress.close();
	    
	    //Toast Message
	    levelUpToast = Toast.makeText(this, "LEVEL UP!", Toast.LENGTH_SHORT);
	    levelUpToast.setGravity(Gravity.BOTTOM|Gravity.LEFT, Main.screenw/5, (int) (Main.screenh/3.3));
	    
	    //2 BUTTONS
	    button1 = (Button)findViewById(R.id.actconunbutton1); button1.setHeight(Main.unith*4); button1.setEnabled(false); button1.setTextSize(Main.fontSize);
	    button2 = (Button)findViewById(R.id.actconunbutton2); button2.setHeight(Main.unith*4); button2.setEnabled(false); button2.setTextSize(Main.fontSize);
	    if(gamemode.equals("practice")){
	    	button1.setText("Back to menu");
	    	button2.setText("Play Again");
	    }else if(gamemode.equals("quick")){
	    	button2.setText("Finish");
	    	button1.setVisibility(View.INVISIBLE);
	    }else{
		    button1.setVisibility(View.INVISIBLE);
		    button2.setText("Progress Report");
	    }
	    
	    //9 LETTERS
	    l1 = (TextView)findViewById(R.id.actconunpointsl1); l1.setWidth(Main.screenw/9); l1.setHeight(Main.screenh/5); l1.setText("" + jumbled.charAt(0)); l1.setTextSize(Main.fontSize); l1.setPadding(1, 1, 1, 1);
	    l2 = (TextView)findViewById(R.id.actconunpointsl2); l2.setWidth(Main.screenw/9); l2.setHeight(Main.screenh/5); l2.setText("" + jumbled.charAt(1)); l2.setTextSize(Main.fontSize); l2.setPadding(1, 1, 1, 1);
	    l3 = (TextView)findViewById(R.id.actconunpointsl3); l3.setWidth(Main.screenw/9); l3.setHeight(Main.screenh/5); l3.setText("" + jumbled.charAt(2)); l3.setTextSize(Main.fontSize); l3.setPadding(1, 1, 1, 1);
	    l4 = (TextView)findViewById(R.id.actconunpointsl4); l4.setWidth(Main.screenw/9); l4.setHeight(Main.screenh/5); l4.setText("" + jumbled.charAt(3)); l4.setTextSize(Main.fontSize); l4.setPadding(1, 1, 1, 1);
	    l5 = (TextView)findViewById(R.id.actconunpointsl5); l5.setWidth(Main.screenw/9); l5.setHeight(Main.screenh/5); l5.setText("" + jumbled.charAt(4)); l5.setTextSize(Main.fontSize); l5.setPadding(1, 1, 1, 1);
	    l6 = (TextView)findViewById(R.id.actconunpointsl6); l6.setWidth(Main.screenw/9); l6.setHeight(Main.screenh/5); l6.setText("" + jumbled.charAt(5)); l6.setTextSize(Main.fontSize); l6.setPadding(1, 1, 1, 1);
	    l7 = (TextView)findViewById(R.id.actconunpointsl7); l7.setWidth(Main.screenw/9); l7.setHeight(Main.screenh/5); l7.setText("" + jumbled.charAt(6)); l7.setTextSize(Main.fontSize); l7.setPadding(1, 1, 1, 1);
	    l8 = (TextView)findViewById(R.id.actconunpointsl8); l8.setWidth(Main.screenw/9); l8.setHeight(Main.screenh/5); l8.setText("" + jumbled.charAt(7)); l8.setTextSize(Main.fontSize); l8.setPadding(1, 1, 1, 1);
	    l9 = (TextView)findViewById(R.id.actconunpointsl9); l9.setWidth(Main.screenw/9); l9.setHeight(Main.screenh/5); l9.setText("" + jumbled.charAt(8)); l9.setTextSize(Main.fontSize); l9.setPadding(1, 1, 1, 1);
	    
	    //INFORM USER
	    text = (TextView)findViewById(R.id.actconpoitext); text.setTextSize(Main.fontSize-10); text.setTypeface(Main.scribble); text.setLineSpacing(1, (float) 0.8);
	    if(answer.equals(conundrum)){
	    	text.setText("Success! '" + answer.toUpperCase() + "' was correct! (+100 Points)");
	    }else if(answer.length()==0){
	    	text.setText("Hard Luck! You had no answer.");
	    }else{
	    	text.setText("Unfortunately '" + answer.toUpperCase() + "' was not correct.");
	    }
	    
	    //SOLUTION VIEW
	    solution = (TextView)findViewById(R.id.actconpoiANSWER); solution.setText("Answer:\n\n" + conundrum.toUpperCase());
	    solution.setTextSize(Main.fontSize-5);
	    
	    //RED BG?
	    TextView BG = (TextView)findViewById(R.id.actconpoiTEXT2C);
	    if(!answer.equals(conundrum)){	//Turn red if answer is incorrect
	    	BG.setBackgroundResource(R.drawable.lightredbutton);
	    	if(sound){
	    		soundPool.play(Main.SNDwrong, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    	}
	    }else{
	    	if(sound){
	    		soundPool.play(Main.SNDsuccess, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    	}
	    }
	    
	    //UPDATE POINTS
	    TextView pointsTexta = (TextView)findViewById(R.id.actconpoiTEXT2A); pointsTexta.setTextSize(Main.fontSize-13);
	    pointstext = (TextView)findViewById(R.id.actconpoiTEXT2B); pointstext.setTextSize(Main.fontSize-13);
	    final int originalPoints = points;
	    int p2a = 0;
	    if(answer.equals(conundrum)){
	    	p2a = 100;
	    }
	    final int points2add = p2a;
	    
	    pointstext.setText(originalPoints + "\n" + points2add + "\n" + bonustimeleft*10 + "\n" + points);
	    if(gamemode.equals("practice")){
			pointstext.setVisibility(View.INVISIBLE);
			findViewById(R.id.actconpoiTEXT2A).setVisibility(View.INVISIBLE);
		}
	   
	   int pluspoints = 0;
	   		if (answer.equals(conundrum)){
	   			pluspoints = points + 100 + (bonustimeleft*10);
	   		}else{
			    pluspoints = points;
	   		}
	   final int newTotal = pluspoints;
	   
	   
	  
	   //LEVEL / TONEXTLEVEL VIEWS
	   levelView = (TextView)findViewById(R.id.actconunLEVEL); levelView.setText("Level: " + lvl); levelView.setTextSize(Main.fontSize-13);
	   toNextLevelView = (TextView)findViewById(R.id.actconunTONEXTLEVEL); 
	   		toNextLevelView.setText(pointscounter + "/" + pointslimit); toNextLevelView.setTextSize(Main.fontSize-13);
	   
	   //PROGRESS BAR ANIMATION
	   progressbar = (ProgressBar)findViewById(R.id.actconunprogressBar); progressbar.setMax(pointslimit); 
	   progressbar.setProgress(pointscounter);
	       	if(!answer.equals(conundrum) || gamemode.equals("practice")){
	       		button1.setEnabled(true); button1.setBackgroundResource(R.drawable.paperbtnpressed); button1.setPadding(1, 1, 1, 1);
	       		button2.setEnabled(true); button2.setBackgroundResource(R.drawable.paperbtnpressed); button2.setPadding(1, 1, 1, 1);   
	       		if(gamemode.equals("practice")){
		       		BG.setVisibility(View.INVISIBLE);
	       		}
	       	}
	       	else{			//...Run the progress bar animation
				updatepoints.postDelayed(new Runnable(){
	    			@Override
	    			public void run() {
	    				updatepoints.postDelayed(this,5);
	    				if(pointscounter==pointslimit){		//Level Up
	    					pointscounter=0;
	    					progressbar.setProgress(pointscounter);
	    					pointstext.setText(originalPoints + "\n" + points2add + "\n" + bonustimeleft*10 + "\n" + points);
	    					if(gamemode.equals("full") || gamemode.equals("quick")){
	    						lvl++;
	    						prefs.edit().putLong("userlvl", lvl).commit();
	    		       			levelView.setText("Level: " + lvl);
	    		       			pointslimit = (int) (lvl*10 + 100);
	    		       			progressbar.setMax(pointslimit);
		    					toNextLevelView.setText(pointscounter + "/" + pointslimit);
		    					//Toast message to inform user of level up
		    					levelUpToast.show();
	    		       		}	
	    					if(sound){
	    						soundPool.play(Main.SNDlevelup, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    					}
	    				}else if(points==newTotal){			//progress bar animation is finished
	    					progressbar.setProgress(pointscounter);
	    					updatepoints.removeCallbacksAndMessages(null);
	    					pointstext.setText(originalPoints + "\n" + points2add + "\n" + bonustimeleft*10 + "\n" + points);
	    					toNextLevelView.setText(pointscounter + "/" + pointslimit);
	    					button1.setEnabled(true); button1.setBackgroundResource(R.drawable.paperbtnpressed); button1.setPadding(1, 1, 1, 1);
	    		       		button2.setEnabled(true); button2.setBackgroundResource(R.drawable.paperbtnpressed); button2.setPadding(1, 1, 1, 1);
	    		       		//Save to shared preferences
	    		       		if(gamemode.equals("full") || gamemode.equals("quick")){
	    		       			prefs.edit().putLong("pointscounter", pointscounter).commit();
	    		       		}
	    		       		if(sound){
	    						soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    					}
	    				}else{
	    					progressbar.setProgress(pointscounter);
	    					pointstext.setText(originalPoints + "\n" + points2add + "\n" + bonustimeleft*10 + "\n" + (points+=1));
	    					pointscounter++;
		    				toNextLevelView.setText(pointscounter + "/" + pointslimit);
	    				}
	    			}
	    		}, 500);
	       	}
	    
	} 
	
	public void actconunmenubutton(View view){
		if(sound){
			soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish(); 
	            	if(sound){
						soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
					}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	public void actconunagainbutton(View view){
		finish();		
		if(sound){
			soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		Intent intent = null;
		if(gamemode.equals("practice")){
			intent = new Intent(this, conundrum.class);		
		}else if(gamemode.equals("quick")){
			intent = new Intent(this, finalScoreScreen.class);
			intent.putExtra("points", points);
		}else{
			intent = new Intent(this, ProgressScreen.class);
			intent.putExtra("points", points);
	    	intent.putExtra("gameround", 15);
		}
    	intent.putExtra("gamemode", gamemode);
    	startActivity(intent);
    	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
	}
	
	public void onBackPressed() {
		//make this an undo button
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish();
	            	if(sound){
						soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
					}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	}
