package com.bryan.barrett.countdownforandroid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class HighScoresBaseModel implements BaseColumns {
    
	protected final String TABLE_NAME;
    protected final Context context;
    protected final SQLiteDatabase database;

    public HighScoresBaseModel(final Context context, final String table) {
        this.context = context;
        TABLE_NAME = table;
        database = new HighScoresDatabase(context).getWritableDatabase();
    }
}