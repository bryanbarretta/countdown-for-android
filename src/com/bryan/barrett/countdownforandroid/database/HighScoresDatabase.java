package com.bryan.barrett.countdownforandroid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
 
public class HighScoresDatabase extends SQLiteOpenHelper{
 
 public HighScoresDatabase(Context context) {
  super(context, "highScores.db", null, 1);
}
 
 @Override
 public void onCreate(SQLiteDatabase db) {
 
  db.execSQL("create table " + HighScoresTable.TABLE_NAME + " (" +
          BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
          HighScoresTable.KEY_TYPE + " TEXT," + 
          HighScoresTable.KEY_POINTS + " INTEGER NOT NULL," +
          HighScoresTable.KEY_NAME + " TEXT," + 
          HighScoresTable.KEY_DATE + " TEXT," +
          //everything below is stored from the progress table in case the user wants to see how they played
          HighScoresTable.KEY_PPOINTS + " TEXT," + 
          HighScoresTable.KEY_PDATASET + " TEXT," +
          HighScoresTable.KEY_PANSWER + " TEXT," + 
          HighScoresTable.KEY_PTARGET + " TEXT);");
 }
 
 @Override
 public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
 }
}