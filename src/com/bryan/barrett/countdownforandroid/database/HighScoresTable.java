package com.bryan.barrett.countdownforandroid.database;
import java.util.Date;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
 
public class HighScoresTable extends HighScoresBaseModel {
 
public final static String TABLE_NAME = "Score";
public static String KEY_TYPE = "type";
public static final String KEY_POINTS = "points";
public static final String KEY_NAME = "name";
public static final String KEY_DATE = "date";
public static final String KEY_PPOINTS = "ppoints";
public static final String KEY_PDATASET = "pdataset";
public static final String KEY_PANSWER = "panswer";
public static final String KEY_PTARGET = "ptarget";

 
 public HighScoresTable(final Context context) {
  super(context, TABLE_NAME);
 }
 
 public Cursor getAllOrdered(String type) {
  return database.query(TABLE_NAME, null, KEY_TYPE + " is '" + type + "'", null, null, null, KEY_POINTS + " DESC");
 }
 
 public String getAllPoints(String gamemode){	 
	  String result = "";
	  Cursor cursor = getAllOrdered(gamemode);
	  for (int i = 0; i < cursor.getCount(); i++) {
		  
		  if(!cursor.moveToPosition(i)){			//break if cursor exceeds last row of table
			  break;
		  }
		  
		  int points = cursor.getInt(cursor.getColumnIndex(HighScoresTable.KEY_POINTS));
		  
		  String valuestring = String.valueOf(points);
		  if(points<100){
			  valuestring = "0" + valuestring;		//add a preceding 0 to scores under 100
		  }
		  result+= " "+valuestring+"\n";
	  	}
	  return result;
	 }

 public String getAllNames(String gamemode){
	  String result = "";
	  Cursor cursor = getAllOrdered(gamemode);
	  for (int i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String name = cursor.getString(cursor.getColumnIndex(HighScoresTable.KEY_NAME)); 
		  

			  result+= name + "\n";
		  
	  }
	  return result;
	 }
 
 public String getAllTimes(String gamemode){
	  String result = "";
	  Cursor cursor = getAllOrdered(gamemode);
	  for (int i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String date = cursor.getString(cursor.getColumnIndex(HighScoresTable.KEY_DATE));
		  
		    result+= date + " \n";
		  
	  }
	  return result;
	 }
 
 public String getAllPPoints(){
	 String result = "";
	  Cursor cursor = getAllOrdered("full");
	  for (int i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String ppoints = cursor.getString(cursor.getColumnIndex(HighScoresTable.KEY_PPOINTS));
		  result+= ppoints + "\n";
	  }
	  return result;
 }
 
 public String getAllPDataset(){
	 String result = "";
	  Cursor cursor = getAllOrdered("full");
	  for (int i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String pdataset = cursor.getString(cursor.getColumnIndex(HighScoresTable.KEY_PDATASET));
		  result+= pdataset + "\n";
		  
	  	}
	  return result;
 }
 
 public String getAllPAnswers(){
	 String result = "";
	  Cursor cursor = getAllOrdered("full");
	  for (int i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String panswers = cursor.getString(cursor.getColumnIndex(HighScoresTable.KEY_PANSWER));
		  result+= panswers + "\n";
		  
	  	}
	  return result;
 }
 
 public String getAllPTargets(){
	 String result = "";
	  Cursor cursor = getAllOrdered("full");
	  for (int i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String ptarget = cursor.getString(cursor.getColumnIndex(HighScoresTable.KEY_PTARGET));
		  result+= ptarget + "\n";
		  
	  	}
	  return result;
 }
 
 public void add(String type, final int points, String name, String ppoints, String pdataset, String panswer, String ptarget) {
  final ContentValues values = new ContentValues();
  Date tmpDate = new Date();
  
  //zero will add a 0 if date is below 10. e.g. 07 instead of 7
  String zero = "";  if(tmpDate.getHours()<10){zero = "0";}; 
  String zero1 = ""; if(tmpDate.getMinutes()<10){zero1 = "0";};
  String zero2 = ""; if(tmpDate.getDate()<10){zero2 = "0";};
  String date = zero + tmpDate.getHours() + ":" + zero1 + tmpDate.getMinutes() + " - " + zero2 +
		  tmpDate.getDate()+"/"+(tmpDate.getMonth()+1)+"/"+(tmpDate.getYear()+1900);
  
  Cursor cursor = getAllOrdered(type);
  
  if(cursor.getCount()<10){	/**IF THERE ARE LESS THAN 10 SCORES SIMPLY ADD THE SCORE**/
	  values.put(KEY_TYPE, type); values.put(KEY_POINTS, points);   values.put(KEY_NAME, name); 
	  values.put(KEY_DATE, date); values.put(KEY_PPOINTS, ppoints); values.put(KEY_PDATASET, pdataset); 
	  values.put(KEY_PANSWER, panswer); values.put(KEY_PTARGET, ptarget);
	  
	  database.insert(TABLE_NAME, null, values);
	  
  }else{	/**OTHERWISE MOVE TO THE LAST SCORE...**/
	  cursor.moveToLast();
	  int last = cursor.getInt(cursor.getColumnIndex(HighScoresTable.KEY_POINTS));
	  if(points<last){	//if value (i.e. points) is less than the lowest points on the table, do nothing
		  return;
	  }else{			//otherwise remove the lowest points entry and add this entry
		  remove(type);
		  values.put(KEY_TYPE, type);
		  values.put(KEY_POINTS, points); values.put(KEY_NAME, name); values.put(KEY_DATE, date);
		  values.put(KEY_PPOINTS, ppoints); values.put(KEY_PDATASET, pdataset); 
		  values.put(KEY_PANSWER, panswer); values.put(KEY_PTARGET, ptarget);
		  database.insert(TABLE_NAME, null, values);
	  }
  }
 }
 
 public void remove(String type) {
	  Cursor cursor = getAllOrdered(type);
	  cursor.moveToLast();
	  int last = cursor.getInt(cursor.getColumnIndex(HighScoresTable.KEY_POINTS));
	  database.delete(TABLE_NAME, KEY_POINTS+"="+last, null);
 }

public String getColPos(int pos, String col) {
	String answer = "";
	Cursor c = getAllOrdered("full");				//order database
	c.moveToPosition(pos);							//move to pos
	answer = c.getString(c.getColumnIndex(col));	//get value in Column col at position pos
	return answer;
}

public int getCount(String type) {
	Cursor cursor = getAllOrdered(type);
	int size = cursor.getCount();
	return size;
}

public void close() {
	database.close();
}
 
}