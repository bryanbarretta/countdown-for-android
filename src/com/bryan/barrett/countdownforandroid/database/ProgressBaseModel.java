package com.bryan.barrett.countdownforandroid.database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bryan.barrett.countdownforandroid.database.ProgressDatabase;

public class ProgressBaseModel implements BaseColumns {

	protected final String progTABLE_NAME;
    protected final Context progcontext;
    protected final SQLiteDatabase progdatabase;

    public ProgressBaseModel(final Context context, final String table) {
        this.progcontext = context;
        progTABLE_NAME = table;
        progdatabase = new ProgressDatabase(context).getWritableDatabase();
    }
}