package com.bryan.barrett.countdownforandroid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
 
public class ProgressDatabase extends SQLiteOpenHelper{
 
 public ProgressDatabase(Context context) {
  super(context, "progress.db", null, 1);
}
 
 @Override
 public void onCreate(SQLiteDatabase db) {
 
  db.execSQL("create table " + ProgressTable.progTABLE_NAME + " (" +
          BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
          ProgressTable.progKEY_VALUE + " INTEGER NOT NULL," + 
          ProgressTable.progKEY_DATA + " TEXT," +
          ProgressTable.progKEY_ANSWER + " TEXT," +
          ProgressTable.progKEY_TARGET + " TEXT);");
 }
 
 @Override
 public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
 }
 
}