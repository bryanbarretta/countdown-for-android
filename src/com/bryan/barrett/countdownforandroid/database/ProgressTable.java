package com.bryan.barrett.countdownforandroid.database;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
 
public class ProgressTable extends ProgressBaseModel {
 
public final static String progTABLE_NAME = "Score";
public static final String progKEY_VALUE = "value";
public static final String progKEY_DATA = "data";
public static final String progKEY_ANSWER = "answer";
public static final String progKEY_TARGET = "target";
 
 public ProgressTable(final Context context) {
  super(context, progTABLE_NAME);
 }
 
 public Cursor getAllOrdered() {
  return progdatabase.query(progTABLE_NAME, null, null, null, null, null, null);
 }
 
 public String getAllRounds(){
	 String result = "";
	 Cursor cursor = getAllOrdered();
	 int i;
	 for (i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  result+= (i+1) + "\n";
	 }
	 return result;
 }
 
 public String getAllPoints(){
	 
	  String result = "";
	  Cursor cursor = getAllOrdered();
	  int i;
	 
	  for (i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  int value = cursor.getInt(cursor.getColumnIndex(ProgressTable.progKEY_VALUE));
		  result+= value + "\n";
	  	}	  
	  return result;
	 }
 
 public String getAllData(){
	  String result = "";
	  Cursor cursor = getAllOrdered();
	  int i; 
	  for (i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String data = cursor.getString(cursor.getColumnIndex(ProgressTable.progKEY_DATA));
		  result+= data + "\n";
	  	}  
	  //have to convert from ArrayList (i.e [A, B, C]) to String (i.e A B C) for screen space
	  result = result.replace("[", ""); result = result.replace("]", "");
	  result = result.replace(",", ""); 
	  return result;
	 }

 public String getAllAnswers(){
	 
	  String result = "";
	  Cursor cursor = getAllOrdered();
	  int i; 
	  for (i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String answer = cursor.getString(cursor.getColumnIndex(ProgressTable.progKEY_ANSWER));
		  result+= answer + "\n";
	  	}  
	  return result;
	 }
 
 public String getAllTargets(){ 
	  String result = "";
	  Cursor cursor = getAllOrdered();
	  int i;
	  for (i = 0; i < cursor.getCount(); i++) {
		  if(!cursor.moveToPosition(i)){
			  break;
		  }
		  String target = cursor.getString(cursor.getColumnIndex(ProgressTable.progKEY_TARGET));	  
		  result+= target + "\n";
	  	}
	  return result;
	 }
 
 public void add(final int value, String data, String answer, String target) {
  final ContentValues values = new ContentValues();
  values.put(progKEY_VALUE, value);
  values.put(progKEY_DATA, data);
  values.put(progKEY_ANSWER, answer);
  values.put(progKEY_TARGET, target);

  progdatabase.insert(progTABLE_NAME, null, values);
 }
 
 public void clear(){
	 progdatabase.delete(progTABLE_NAME, null, null);
 }

public void close() {
	progdatabase.close();
	
}
 
}