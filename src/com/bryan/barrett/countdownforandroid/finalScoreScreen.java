package com.bryan.barrett.countdownforandroid;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.HighScoresTable;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class finalScoreScreen extends Activity {

	String gamemode, username;
	int points;
	EditText name;
	Button button;
	HighScoresTable scoresTable;
	ProgressTable progress;
	SharedPreferences prefs;
	Toast errorToast;
	SoundPool soundPool;
	AudioManager audio;
	boolean sound;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_final_score_screen);

	    //db
	    scoresTable = new HighScoresTable(this);
	    
	    //Toast Message
	    errorToast = Toast.makeText(this, "Name must be between 3 and 8 characters long", Toast.LENGTH_SHORT);
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    points = i.getIntExtra("points", 0);
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    username = prefs.getString("username", "");
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    TextView title = (TextView)findViewById(R.id.finalscreentitle); title.setTypeface(Main.scribble); title.setTextSize(Main.fontSize+8);
	    name = (EditText)findViewById(R.id.finalscreenname); name.setWidth(Main.screenw/2); name.setHeight(Main.unith*4);
	    name.setText(username); name.setTextSize(Main.fontSize-10);
	    
	    TextView score = (TextView)findViewById(R.id.finalscreenscore); score.setWidth(Main.screenw/2);
    	score.setText("Game Mode: " + gamemode + "\nScore: " + points); score.setTextSize(Main.fontSize-10);
    	    
	    button = (Button)findViewById(R.id.finalscreenbutton); button.setWidth(Main.screenw/2); button.setHeight(Main.unith*4);
	    button.setTextSize(Main.fontSize);
	    Button buttonrate = (Button)findViewById(R.id.rateButton); buttonrate.setTextSize(Main.fontSize); buttonrate.setMinHeight(Main.unith*4);
	}
	
	public void finalscreenbutton(View view){
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		progress = new ProgressTable(this);
		username = name.getText().toString();
		if(username.length() > 2 && username.length() < 9){
			prefs.edit().putString("username", username).commit();
			if(gamemode.equals("quick")){
				scoresTable.add(gamemode, points, username, null, null, null, null);
			}else if(gamemode.equals("full")){
				scoresTable.add(gamemode, points, username, progress.getAllPoints(),
				progress.getAllData(), progress.getAllAnswers(), progress.getAllTargets());
				progress.clear();
			}
			finish();
		}else{
			errorToast.show();
		}
	    progress.close(); scoresTable.close();
	}
	
	public void rateClicked(View view){
		Uri uri = Uri.parse("market://details?id=com.bryan.barrett.countdownforandroid");
	    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
	    try {
	        startActivity(goToMarket);
	    } catch (ActivityNotFoundException e) {
	    	Toast toast = Toast.makeText(this, "Cannot Connect to Android Market at this time. Please try again later!", Toast.LENGTH_SHORT);
		    toast.setGravity(Gravity.BOTTOM|Gravity.LEFT, 5, 5);
		    toast.show();
	    }
	}
	

}
