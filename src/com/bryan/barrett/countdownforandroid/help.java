package com.bryan.barrett.countdownforandroid;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import com.bryan.barrett.countdownforandroid.R;
import android.preference.PreferenceManager;
import android.widget.TextView;

public class help extends Activity {

	boolean sound;
	MediaPlayer back;
	SharedPreferences prefs;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.help);
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    back = MediaPlayer.create(getApplicationContext(), R.raw.woosh);
    	
	    TextView mainTitle = (TextView)findViewById(R.id.helpTitle);
    	mainTitle.setTypeface(Main.scribble);
	}
	
	public void onBackPressed() {
	    super.onBackPressed();
	    if(sound){
	    	back.start();
	    }
	}
}
