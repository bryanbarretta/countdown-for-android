package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;
import java.util.Collections;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class lettersCreate extends Activity implements SensorListener{

	String gamemode;
	ArrayList<String> myletters = new ArrayList<String>();
	ArrayList<String> mywordlist = new ArrayList<String>();
	ArrayList<String> currentword = new ArrayList<String>();
	ArrayList<Integer> btnIDlist = new ArrayList<Integer>();
	static ArrayList<String> bestwords = new ArrayList<String>();
	boolean sound, mUserPresent = true;
	
	int counter = 33, letcount = 0, points, gameround;
	
	Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
	LinearLayout clear, undo, addtolist;
	ArrayList<Button> buttons = new ArrayList<Button>();
	TextView timertext, l1, l2, l3, l4, l5, l6, l7, l8, l9, mywords;
	ArrayList<TextView> letters = new ArrayList<TextView>();
	Handler timer = new Handler();
	Runnable runnable;
	
	//Database / Memory
	ProgressTable progress;	
	SharedPreferences prefs;
	
	//Shake / Vibrator
	SensorManager sensorMgr;
	Vibrator v;
	long lastUpdate;
	float last_x = 0, last_y = 0, last_z = 0, x = 0, y = 0, z = 0;
	private static final int SHAKE_THRESHOLD = 800;
	
	SoundPool soundPool;
	BroadcastReceiver br;
	boolean screenOn = true;
	AudioManager audio;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_letters_create);
	    bgTask.setContext(getApplicationContext());	//passes this context onto bgTask 
	    											//(It has not context because it does not extend Activity)
    	
	    //set up a BROADCAST RECEIVER and register it
	    br = new BroadcastReceiver(){
	    	@Override
	        public void onReceive(Context context, Intent intent) {
	    		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
		            //What to do if screen is turned off
	    			screenOn = false;}
		        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
		        	//What to do if user is present at app
		        	screenOn = true;
		        	onResume();
		        }
	        }
	    };
	    IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
	    filter.addAction(Intent.ACTION_SCREEN_OFF);
	    registerReceiver(br, filter);
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    points = i.getIntExtra("points", 0);
	    gameround = i.getIntExtra("gameround", 0);
	    myletters = i.getStringArrayListExtra("myletters");
	    
	    //sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	   
	    timertext = (TextView)findViewById(R.id.actletcretimertext); timertext.setTypeface(Main.led); 
	    timertext.setHeight(Main.unith*4); timertext.setTextSize(Main.fontSize+20);
	    
	    btn1 = (Button)findViewById(R.id.actletcrebtn1); buttons.add(btn1);
	    btn2 = (Button)findViewById(R.id.actletcrebtn2); buttons.add(btn2);
	    btn3 = (Button)findViewById(R.id.actletcrebtn3); buttons.add(btn3);
	    btn4 = (Button)findViewById(R.id.actletcrebtn4); buttons.add(btn4);
	    btn5 = (Button)findViewById(R.id.actletcrebtn5); buttons.add(btn5);
	    btn6 = (Button)findViewById(R.id.actletcrebtn6); buttons.add(btn6);
	    btn7 = (Button)findViewById(R.id.actletcrebtn7); buttons.add(btn7);
	    btn8 = (Button)findViewById(R.id.actletcrebtn8); buttons.add(btn8);
	    btn9 = (Button)findViewById(R.id.actletcrebtn9); buttons.add(btn9);
	    for(int b = 0; b < 9; b++){
	    	buttons.get(b).setWidth(Main.screenw/9); buttons.get(b).setHeight(Main.unith*4); buttons.get(b).setPadding(2, 2, 2, 2);
	    	buttons.get(b).setText(myletters.get(b)); buttons.get(b).setTextSize(Main.fontSize);
	    }
	    l1 = (TextView)findViewById(R.id.actletcrel1); letters.add(l1);
	    l2 = (TextView)findViewById(R.id.actletcrel2); letters.add(l2);
	    l3 = (TextView)findViewById(R.id.actletcrel3); letters.add(l3);
	    l4 = (TextView)findViewById(R.id.actletcrel4); letters.add(l4);
	    l5 = (TextView)findViewById(R.id.actletcrel5); letters.add(l5);
	    l6 = (TextView)findViewById(R.id.actletcrel6); letters.add(l6);
	    l7 = (TextView)findViewById(R.id.actletcrel7); letters.add(l7);
	    l8 = (TextView)findViewById(R.id.actletcrel8); letters.add(l8);
	    l9 = (TextView)findViewById(R.id.actletcrel9); letters.add(l9);
	    for(int l = 0; l < 9; l++){
	    	letters.get(l).setWidth(Main.screenw/9); letters.get(l).setHeight(Main.unith*4); letters.get(l).setTextSize(Main.fontSize);
	    }
	    
	    mywords = (TextView)findViewById(R.id.actletcremywordstext); mywords.setTypeface(Main.scribble);
	    mywords.setTextSize(Main.fontSize-8); mywords.setLineSpacing(1, (float) 0.8);
	    
	    //Command buttons
	    clear = (LinearLayout)findViewById(R.id.actletcreclear);
	    	TextView clrTXT = (TextView)findViewById(R.id.actletcreclearTXT); clrTXT.setTextSize(Main.fontSize-10);
	    undo = (LinearLayout)findViewById(R.id.actletcreundo);
	    	android.view.ViewGroup.LayoutParams params = undo.getLayoutParams(); params.height = Main.unith*4;
	    	TextView undTXT = (TextView)findViewById(R.id.actletcreundoTXT); undTXT.setTextSize(Main.fontSize-10);
	    addtolist = (LinearLayout)findViewById(R.id.actletcreaddtolist);
	    	TextView addlistTXT = (TextView)findViewById(R.id.actletcreaddlistTXT); addlistTXT.setTextSize(Main.fontSize-10);
	    	
	    //runnable for the 30 second timer. starts in onResume() 
		runnable = new Runnable(){
            @Override
            public void run() {
            	timer.postDelayed(this,1000);
                if(counter == 33){
                	if(sound){
                		soundPool.play(Main.SNDbeep1, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                	}
                	timertext.setClickable(false);
                	timertext.setTextColor(Color.RED);
                	timertext.setText("Ready?"); 
                	//calculate a best word
            	    String word2solve = "";
            	    for(int a = 0; a < myletters.size(); a++){
            	    	word2solve += myletters.get(a);
            	    }
            		new bgTask().execute(word2solve);
            		
                }else if(counter == 32){
                	if(sound){
                		soundPool.play(Main.SNDbeep1, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                	}
                	timertext.setTextColor(Color.rgb(255, 150, 0));
                	timertext.setText("Set!");
                }else if(counter == 31){
                	if(sound){
                		soundPool.play(Main.SNDbeep2, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                	}
                	btn1.setEnabled(true);btn2.setEnabled(true);btn3.setEnabled(true);btn4.setEnabled(true);
                	btn5.setEnabled(true);btn6.setEnabled(true);btn7.setEnabled(true);btn8.setEnabled(true);btn9.setEnabled(true);
                	timertext.setTextColor(Color.GREEN);
                	timertext.setText("GO!!");
                }else if(counter == -1){
                	timertext.setText("TIME UP"); 
                	
                	btn1.setEnabled(false);btn2.setEnabled(false);btn3.setEnabled(false);
                	btn4.setEnabled(false);btn5.setEnabled(false);btn6.setEnabled(false);
                	btn7.setEnabled(false);btn8.setEnabled(false);btn9.setEnabled(false);
                }
                else if(counter == -2){
                	if(mywordlist.isEmpty()||(mywordlist.size()==1)){
                		skipRound(mywordlist);
                	}else{
                		nextRound(mywordlist);
                	}
                }else{
                	timertext.setTextColor(Color.RED);
                	if(sound){
                		if(counter <= 30 && counter > 5){
                			soundPool.play(Main.SNDtick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
        							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                		}else if(counter <= 5 && counter >= 1){
                			soundPool.play(Main.SNDtickFinal, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
        							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                		}else if(counter == 0){
                			soundPool.play(Main.SNDtimeup, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
        							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                		}
                	}
                	if(counter>9){
                	timertext.setText("00:" + counter);
                	}else{
                		timertext.setText("00:0" + counter);	
                	}
                }                
                counter--;
            }
	    };
	}

	
	public void buttonPressed1(View view){String letter = myletters.get(0);int btnID = 0;btnpressed(btn1, letter, btnID);}
	public void buttonPressed2(View view){String letter = myletters.get(1);int btnID = 1;btnpressed(btn2, letter, btnID);}
	public void buttonPressed3(View view){String letter = myletters.get(2);int btnID = 2;btnpressed(btn3, letter, btnID);}
	public void buttonPressed4(View view){String letter = myletters.get(3);int btnID = 3;btnpressed(btn4, letter, btnID);}
	public void buttonPressed5(View view){String letter = myletters.get(4);int btnID = 4;btnpressed(btn5, letter, btnID);}
	public void buttonPressed6(View view){String letter = myletters.get(5);int btnID = 5;btnpressed(btn6, letter, btnID);}
	public void buttonPressed7(View view){String letter = myletters.get(6);int btnID = 6;btnpressed(btn7, letter, btnID);}
	public void buttonPressed8(View view){String letter = myletters.get(7);int btnID = 7;btnpressed(btn8, letter, btnID);}
	public void buttonPressed9(View view){String letter = myletters.get(8);int btnID = 8;btnpressed(btn9, letter, btnID);}
	
	public void clearbuttonpressed(View view){
		if(letcount == 0){
			return;
		}else{
			if(sound == true){
				soundPool.play(Main.SNDshuffle, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
			}
			letcount = 0;
			resetLetters();
			resetButtons();
			for(int i = 0; i <= 8; i++){
				currentword.clear();
				btnIDlist.clear();
			}
		}
	}
	
	public void undobuttonpressed(View view){
		if(btnIDlist.isEmpty()){
			return;
		}else{
			if(sound == true){
				if (Main.loaded) {
					soundPool.play(Main.SNDundo, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
			      }
			}
			letcount--;
			letters.get(letcount).setVisibility(View.INVISIBLE);
			letters.get(letcount).setText("");
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setBackgroundResource(R.drawable.smalltileonclick);
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setText("" + myletters.get(btnIDlist.get(btnIDlist.size()-1)));
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setPadding(1, 1, 1, 1);
			buttons.get(btnIDlist.get(btnIDlist.size()-1)).setEnabled(true);
			btnIDlist.remove(btnIDlist.size()-1);
			currentword.remove(currentword.size()-1);
		}
	}
	
	public void addtolistbuttonpressed(View view){
		if(currentword.isEmpty()){
			return;
		}else{
			if(sound == true){
				soundPool.play(Main.SNDwriting, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);			
			}
			letcount = 0;
			StringBuilder thiswordbuild= new StringBuilder();
			//letters
			for (int i = 0; i < currentword.size(); i++){
				thiswordbuild.append(currentword.get(i));
			}
			resetLetters();
			resetButtons();
			String thisword = thiswordbuild.toString();
			mywords.setText(mywords.getText() + "(" + thisword.length() + ") " + thisword + ", ");
			mywordlist.add(thisword);
			currentword.clear();
			btnIDlist.clear();
		}
	}
	
	private void btnpressed(Button btn, String letter, int btnID) {
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC),
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		btnIDlist.add(btnID);
		currentword.add(letter);
		buttons.get(btnID).setBackgroundResource(R.drawable.slotempty);	//change view resource
		buttons.get(btnID).setPadding(1, 1, 1, 1);						//define padding
		buttons.get(btnID).setText("");									//remove text from view
		buttons.get(btnID).setEnabled(false);							
		letters.get(letcount).setVisibility(View.VISIBLE);
		letters.get(letcount).setPadding(1, 1, 1, 1);
		letters.get(letcount).setText("" + letter);
		letcount++;
	}
	
	public void resetButtons(){
		//buttons
		for (int j = 0; j <= 8; j++){
			buttons.get(j).setBackgroundResource(R.drawable.smalltileonclick);
			buttons.get(j).setText(myletters.get(j));
			buttons.get(j).setPadding(1, 1, 1, 1);
			buttons.get(j).setEnabled(true);
		}
	}
	
	public void resetLetters(){
		//letters
		for (int i = 0; i < currentword.size(); i++){
			letters.get(i).setVisibility(View.INVISIBLE);
			letters.get(i).setText("");
		}
	}

	public void onPause(){
		super.onPause(); 
		sensorMgr.unregisterListener(this);	//Stop Sensor
		v.cancel();							//Stop Vibrator
		timer.removeCallbacks(runnable);	//removes runnable from timer handler. counter will not reset.
	}
	
	public void onResume(){
		super.onResume();
		if(screenOn == true){
			//Sensor
			sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
			sensorMgr.registerListener(this, SensorManager.SENSOR_ACCELEROMETER, SensorManager.SENSOR_DELAY_GAME);
			//Vibrator
			v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			timer.post(runnable);
		}
	}

	public void onBackPressed() {
		progress = new ProgressTable(this);
		
		//make this an undo button?
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
								audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            	onFinish();
	            	if(gamemode.equals("full")){
	            		progress.add(0, myletters + "", "*forfeited", "");
	            	}
            	    progress.close();
	            }
	            case DialogInterface.BUTTON_NEGATIVE:

            	    progress.close();
	                return;
	            }
	        }
	    };
	    String question = "";
	    if(gamemode.equals("full")){
	    	question = "Are you sure you want to quit? (You will forfeit the round)";
	    }else{
	    	question = "Return to menu?";
	    }
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(question).setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	private void nextRound(ArrayList<String> mywordlist) {
		Intent intent = new Intent(this, lettersSubmit.class);
		intent.putStringArrayListExtra("mywordlist", mywordlist);
    	intent.putExtra("points", points);
		intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);
    	intent.putStringArrayListExtra("bestwords", bestwords);
		intent.putStringArrayListExtra("myletters", myletters);
    	startActivity(intent);
   	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	onFinish();
    	timer.removeCallbacksAndMessages(null);
	}
	
	private void skipRound(ArrayList<String> mywordlist) {
		Intent intent = new Intent(this, lettersPoints.class);
		String finalword;
		if(mywordlist.isEmpty()){
			finalword = "";
		}else{
			finalword = mywordlist.get(0);
		}
		intent.putExtra("finalword", finalword);
    	intent.putExtra("points", points);
		intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);
    	intent.putStringArrayListExtra("bestwords", bestwords);
		intent.putStringArrayListExtra("myletters", myletters);
    	startActivity(intent);
   	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	onFinish();
	}
	
	public static void setBestWord(ArrayList<String> theBestWords){
		bestwords = theBestWords;
	}


	@Override
	public void onAccuracyChanged(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onSensorChanged(int sensor, float[] values) {
		if (sensor == SensorManager.SENSOR_ACCELEROMETER) {
		    long curTime = System.currentTimeMillis();
		    // only allow one update every 200ms.
		    if ((curTime - lastUpdate) > 200) {
		      long diffTime = (curTime - lastUpdate);
		      lastUpdate = curTime;

		      x = values[SensorManager.DATA_X];
		      y = values[SensorManager.DATA_Y];
		      z = values[SensorManager.DATA_Z];

		      //x+y+z � last_x � last_y � last_z
		      
		      float speed = Math.abs(x+y+z-last_z-last_y-last_x) / diffTime * 10000;

		      if (speed > SHAKE_THRESHOLD) {	//Device is shook
		        //Vibrate Device
		    	v.vibrate(200);
		    	//Clear 
		    	letcount = 0;
				resetLetters();
				resetButtons();
				for(int i = 0; i <= 8; i++){
					currentword.clear();
					btnIDlist.clear();
				}
				//Shuffle Letters and update UI
		        Collections.shuffle(myletters);
		        for(int nn = 0; nn < 9; nn++){
		        	buttons.get(nn).setText(myletters.get(nn));
		        }
		        if(sound == true){
		        	soundPool.play(Main.SNDshuffle, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
							audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		        }
		      }
		      last_x = x;
		      last_y = y;
		      last_z = z;
		    }
		  }
	}
	
	public void onFinish(){
		unregisterReceiver(br);
		sensorMgr.unregisterListener(this);
		v.cancel();
		timer.removeCallbacksAndMessages(null);
		finish();
	}
}
