package com.bryan.barrett.countdownforandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class lettersPoints extends Activity {

	String gamemode, finalword;
	long lvl;
	ArrayList<String> myletters = new ArrayList<String>();
	ArrayList<String> bestwords = new ArrayList<String>();
	InputStream dictionary;
	int points, pointsnew=0, pointscounter=0, gameround, pointslimit;
	boolean exists=false;
	Handler updatepoints= new Handler();
	ProgressTable progress;
	Toast levelUpToast;
	
	TextView l1, l2, l3, l4, l5, l6, l7, l8, l9, usertext, barText, pointsText, toNextLevelText, 
		bestwordText;
	Button button1, button2;
	ProgressBar progressbar;
	SharedPreferences prefs;
	
	SoundPool soundPool;
	AudioManager audio;
	boolean sound;
	
	/** Called when the activity is first created. */
	@SuppressLint("ShowToast")
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_letters_points);
	    
	    //db
	    progress = new ProgressTable(this);
	    
	    //Shared Preferences
	    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    lvl = prefs.getLong("userlvl", 1);
	    pointslimit = (int) (lvl*10 + 100);
	    pointscounter = (int) prefs.getLong("pointscounter", 0);
	    sound = prefs.getBoolean("sound", true);
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    finalword = i.getStringExtra("finalword");
	    exists = checkword(finalword);
	    myletters = i.getStringArrayListExtra("myletters");
	    points = i.getIntExtra("points", 0);
	    gameround = i.getIntExtra("gameround", 0);
	    bestwords = i.getStringArrayListExtra("bestwords");
	    
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //Toast Message
	    levelUpToast = Toast.makeText(this, "LEVEL UP!", Toast.LENGTH_SHORT);
	    levelUpToast.setGravity(Gravity.BOTTOM|Gravity.LEFT, Main.screenw/5, (int) (Main.screenh/3.3));
	    
	    //LETTERS
	    l1 = (TextView)findViewById(R.id.actletpoi1); l1.setWidth(Main.screenw/9); l1.setHeight(Main.unith*4); l1.setText(myletters.get(0)); l1.setTextSize(Main.fontSize);l1.setPadding(1, 1, 1, 1);
	    l2 = (TextView)findViewById(R.id.actletpoi2); l2.setWidth(Main.screenw/9); l2.setHeight(Main.unith*4); l2.setText(myletters.get(1)); l2.setTextSize(Main.fontSize);l2.setPadding(1, 1, 1, 1);
	    l3 = (TextView)findViewById(R.id.actletpoi3); l3.setWidth(Main.screenw/9); l3.setHeight(Main.unith*4); l3.setText(myletters.get(2)); l3.setTextSize(Main.fontSize);l3.setPadding(1, 1, 1, 1);
	    l4 = (TextView)findViewById(R.id.actletpoi4); l4.setWidth(Main.screenw/9); l4.setHeight(Main.unith*4); l4.setText(myletters.get(3)); l4.setTextSize(Main.fontSize);l4.setPadding(1, 1, 1, 1);
	    l5 = (TextView)findViewById(R.id.actletpoi5); l5.setWidth(Main.screenw/9); l5.setHeight(Main.unith*4); l5.setText(myletters.get(4)); l5.setTextSize(Main.fontSize);l5.setPadding(1, 1, 1, 1);
	    l6 = (TextView)findViewById(R.id.actletpoi6); l6.setWidth(Main.screenw/9); l6.setHeight(Main.unith*4); l6.setText(myletters.get(5)); l6.setTextSize(Main.fontSize);l6.setPadding(1, 1, 1, 1);
	    l7 = (TextView)findViewById(R.id.actletpoi7); l7.setWidth(Main.screenw/9); l7.setHeight(Main.unith*4); l7.setText(myletters.get(6)); l7.setTextSize(Main.fontSize);l7.setPadding(1, 1, 1, 1);
	    l8 = (TextView)findViewById(R.id.actletpoi8); l8.setWidth(Main.screenw/9); l8.setHeight(Main.unith*4); l8.setText(myletters.get(7)); l8.setTextSize(Main.fontSize);l8.setPadding(1, 1, 1, 1);
	    l9 = (TextView)findViewById(R.id.actletpoi9); l9.setWidth(Main.screenw/9); l9.setHeight(Main.unith*4); l9.setText(myletters.get(8)); l9.setTextSize(Main.fontSize);l9.setPadding(1, 1, 1, 1);
	    
	    //BUTTONS: SET TEXT BASED ON GAMEMODE / GAMEROUND
	    button1 = (Button)findViewById(R.id.actletpoibutton1); button1.setHeight(Main.unith*4); button1.setEnabled(false); button1.setTextSize(Main.fontSize);
	    button2 = (Button)findViewById(R.id.actletpoibutton2); button2.setHeight(Main.unith*4); button2.setEnabled(false); button2.setTextSize(Main.fontSize);
	    if(gamemode.equals("practice")){
	    	button1.setText("Back to menu");
	    	button2.setText("Play Again");
	    }else if(gamemode.equals("quick")){
	    	button1.setText("Quit");
	    	button2.setText("Numbers Round");
	    }else if(gamemode.equals("full")){
	    	if(gameround == 4 || gameround == 9 || gameround == 13){
		    	button1.setText("Save & Quit");
		    	button2.setText("Round " + (gameround+1) + ": Numbers");	    		
	    	}else{
		    	button1.setText("Save & Quit");
		    	button2.setText("Round " + (gameround+1) + ": Letters");	 
	    	}
	    }
	    
	    //EXP BOX: GREEN IF WORD EXISTS. TURN RED IF IT DOES NOT
	    TextView EXPview = (TextView)findViewById(R.id.actletpoiLEFTtext2C); 
	    if(exists==false || finalword.length()==0){	//User gets 0 points
	    	EXPview.setBackgroundResource(R.drawable.lightredbutton);
	    	if(sound){
        		soundPool.play(Main.SNDwrong, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
        	}
	    }else{										//User gets points
	    	if(sound){
        		soundPool.play(Main.SNDsuccess, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
        	}
	    }
	     
	    //TEXTVIEW: INFORM USER IF THEIR WORD EXISTS
	    usertext = (TextView)findViewById(R.id.actletpoiLEFTtext1); usertext.setTextSize(Main.fontSize-10); usertext.setTypeface(Main.scribble); usertext.setLineSpacing(1, (float) 0.8);
	    if(finalword.length()==0){
	    	usertext.setText("Hard luck. You had no word.");
	    	if(gamemode.equals("full")){
	    			progress.add(0, myletters+"", "-", "(" + bestwords.get(0).length() + ")." + bestwords.get(0));
	    	}
	    }else{
	    	if (exists == false){
	    		usertext.setText("Unfortunately " + finalword.toUpperCase() + " is not in the dictionary.");
	    	}else{
	    		usertext.setText("Success! " + finalword.toUpperCase() + " is in the dictionary. (+" + getpoints() + " Points)");
	    	}
	    	if(gamemode.equals("full")){
	    			progress.add(getpoints(), myletters.toString(), "(" + finalword.length() + ")." 
	    					+ finalword.toUpperCase(), "(" + bestwords.get(0).length() + ")." + bestwords.get(0).toUpperCase());
	    	}
	    }
	    progress.close();
	    
	    //POINTS TEXT (UPDATED IN PROGRESS BAR ANIMATION)
	    TextView pointsTexta = (TextView)findViewById(R.id.actletpoiLEFTtext2A); pointsTexta.setTextSize(Main.fontSize-13);
	    pointsText = (TextView)findViewById(R.id.actletpoiLEFTtext2B); pointsText.setTextSize(Main.fontSize-13);
	    final int originalPoints = points;
	    final int plusPoints = getpoints();
	    pointsText.setText(originalPoints + "\n" + plusPoints + "\n" + points);
	    if(gamemode.equals("practice")){
			pointsText.setVisibility(View.INVISIBLE);
			findViewById(R.id.actletpoiLEFTtext2A).setVisibility(View.INVISIBLE);
		}
	    
	    //PROGRESS BAR TEXT
	    barText = (TextView)findViewById(R.id.actletpoiBARtext); barText.setText("Level: " + lvl); barText.setTextSize(Main.fontSize-13);
	    toNextLevelText = (TextView)findViewById(R.id.actletpoiBARtoNextLevel); 
	    	toNextLevelText.setText(pointscounter + "/" + pointslimit); toNextLevelText.setTextSize(Main.fontSize-13);
	    
	    //PROGRESS BAR ANIMATION
	    progressbar = (ProgressBar)findViewById(R.id.actletpoiBAR); progressbar.setMax(pointslimit);
	    progressbar.setProgress(pointscounter);
	    
	    final int newTotal = points + getpoints();
	    if(getpoints()==pointslimit){
	   		pointscounter++;
	   		points+=1;
	   	}
	    progressbar.setProgress(pointscounter);
	       	if(exists==false || gamemode.equals("practice")){	//...Skip the progress bar animation
	       		button1.setEnabled(true); button1.setBackgroundResource(R.drawable.paperbtnpressed); button1.setPadding(1, 1, 1, 1);
	       		button2.setEnabled(true); button2.setBackgroundResource(R.drawable.paperbtnpressed); button2.setPadding(1, 1, 1, 1);
	       		if(gamemode.equals("practice")){
	       			EXPview.setVisibility(View.INVISIBLE);
	       		}
	       	}else{	
	       		updatepoints.postDelayed(new Runnable(){
	    			@Override
	    			public void run() {
	    				updatepoints.postDelayed(this,5);
	    				if(pointscounter == pointslimit){	//level up
	    					pointscounter=0;
	    					progressbar.setProgress(pointscounter);
	    					pointsText.setText(originalPoints + "\n" + plusPoints + "\n" + points);
	    					//Save level to shared preferences
	    					if(gamemode.equals("full") || gamemode.equals("quick")){
	    						lvl++;
	    						prefs.edit().putLong("userlvl", lvl).commit();
	    		       			//getPreferences(MODE_PRIVATE).edit().putLong("userlvl", lvl).commit();
	    		       			barText.setText("Level: " + lvl);
	    		       			pointslimit = (int) (lvl*10 + 100);
	    		       			progressbar.setMax(pointslimit);
		    					toNextLevelText.setText(pointscounter + "/" + pointslimit);
		    					//Toast message to inform user of level up
		    					levelUpToast.show();
		    					if(sound){
		    		        		soundPool.play(Main.SNDlevelup, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
		    								audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		    		        	}
	    		       		}	    					
	    				}else if(points==newTotal){	//progress bar animation is finished
	    					progressbar.setProgress(pointscounter);
	    					updatepoints.removeCallbacksAndMessages(null);
	    					pointsText.setText(originalPoints + "\n" + plusPoints + "\n" + points);
	    					toNextLevelText.setText(pointscounter + "/" + pointslimit);
	    		       		button1.setEnabled(true); button1.setBackgroundResource(R.drawable.paperbtnpressed); button1.setPadding(1, 1, 1, 1);
	    		       		button2.setEnabled(true); button2.setBackgroundResource(R.drawable.paperbtnpressed); button2.setPadding(1, 1, 1, 1);
	    		       		//Save to shared preferences
	    		       		if(gamemode.equals("full") || gamemode.equals("quick")){
	    		       			prefs.edit().putLong("pointscounter", pointscounter).commit();
	    		       		}
							//...Run the progress bar animation
	    		       		if(sound){
	    		       			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    		       					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    		       		}
	    				}else{
	    					progressbar.setProgress(pointscounter);
	    					pointsText.setText(originalPoints + "\n" + plusPoints + "\n" + (points += 1));
	    					pointscounter++;
	    					toNextLevelText.setText(pointscounter + "/" + pointslimit);
	    				}
	    			}
	    		}, 500);
	       	}
	    
	      //TEXTVIEW: RIGHT BACKGROUND BEST POSSIBLE WORDS
		    bestwordText = (TextView)findViewById(R.id.actletpoiBGRIGHTtext); bestwordText.setTextSize(Main.fontSize-10); 
		    	bestwordText.setTypeface(Main.scribble); bestwordText.setLineSpacing(1, (float) 0.8);
		    String bestwordTextString = "Best Words (scroll):\n";
		    int prevSize = 0;
		    for(int q = 0; q < bestwords.size(); q++){
		    	if(bestwords.get(q).length() != prevSize){
		    		bestwordTextString += "\n";
		    	}
		    	bestwordTextString += "(" + bestwords.get(q).length() + ") " + bestwords.get(q).toUpperCase() + "\n";
		    	prevSize = bestwords.get(q).length();
		    }
		    bestwordText.setText(bestwordTextString);
	}
	
	private int getpoints() {
		int wordsize = finalword.length();
		int points = 0;
		if(exists==false){
			points = 0;
		}else if(wordsize == 9){
			points = 180;
		}else{
			points = wordsize * 10;
		}
		
		return points;	
	}
	
	public void actletpoimenubutton(View view){
		if(sound){
			soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish(); 
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
								audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	public void actletpoiagainbutton(View view){
		finish();
		if(sound){
			soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		Intent intent = null;
		if(gamemode.equals("practice")){
			intent = new Intent(this, lettersSelect.class);
			points = 0;
		}else if(gamemode.equals("quick")){
			intent = new Intent(this, numbersSelect.class);
		}else {
			if(gameround == 4 || gameround == 9 || gameround == 13){
				intent = new Intent(this, numbersSelect.class);				
			}else{
				intent = new Intent(this, lettersSelect.class);							
			}
		}
    	intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);
    	intent.putExtra("points", points);
    	startActivity(intent);
    	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
	}

	public void onBackPressed() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish(); 
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	//------------------------------CHECK DICTIONARY FOR USERS WORD----------------------------------
	
	private boolean checkword(String finalword){
		String userword = finalword.toLowerCase();
		boolean b;
		
		if(Collections.binarySearch(Main.dictionary, userword)>0){
			b = true;
		}else{
			b = false;
		}
		
		return b;		
	}

	
}
