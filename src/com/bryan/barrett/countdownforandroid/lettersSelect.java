package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.bryan.barrett.countdownforandroid.R;

public class lettersSelect extends Activity {

	TextView header, letter5, letter1, letter2, letter3, letter4, letter6, letter7, letter8, letter9, border;
	Button consbut, vowelbut;
	
	String vowels = "AAAAAEEEEEIIIOOUU";
	String consonants = "BBCCCDDDDDDFFGGGHHJKLLLLLMMMMNNNNNNNNPPPPQRRRRRRRRRSSSSSSSSSTTTTTTTTVWXYZ";
	String gamemode;
	int conscount, vowelcount, letcount, secondsleft, points, gameround;
	ArrayList<String> myletters = new ArrayList<String>();
	ArrayList<TextView> letters = new ArrayList<TextView>();
	Random rand = new Random();
	Handler timeout = new Handler();
	Runnable runnable;
	SharedPreferences prefs;
	boolean sound, screenOn = true;
	SoundPool soundPool;
	AudioManager audio;
	BroadcastReceiver br;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_letters_select);
	    conscount = vowelcount = letcount = 0; secondsleft = 20;
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    gameround = i.getIntExtra("gameround", 0) + 1;
	    points = i.getIntExtra("points", 0);
	    
	    //set up a BROADCAST RECEIVER and register it
	    br = new BroadcastReceiver(){
	    	@Override
	        public void onReceive(Context context, Intent intent) {
	    		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
		            //What to do if screen is turned off
	    			screenOn = false;}
		        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
		        	//What to do if user is present at app
		        	screenOn = true;
		        	onResume();
		        }
	        }
	    };
	    IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
	    filter.addAction(Intent.ACTION_SCREEN_OFF);
	    registerReceiver(br, filter);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    header = (TextView)findViewById(R.id.actletselheader); header.setWidth(Main.screenw); header.setHeight(Main.unith*5);
	    	header.setTypeface(Main.scribble); header.setTextSize(Main.fontSize+5);
	    if(gamemode.equals("full")){
	    	header.setText("Round " + gameround + ": Select 9 Letters");
	    }
	    letter1 = (TextView)findViewById(R.id.actletselletter1); letters.add(letter1);
	    letter2 = (TextView)findViewById(R.id.actletselletter2); letters.add(letter2);
	    letter3 = (TextView)findViewById(R.id.actletselletter3); letters.add(letter3);
	    letter4 = (TextView)findViewById(R.id.actletselletter4); letters.add(letter4);
	    letter5 = (TextView)findViewById(R.id.actletselletter5); letters.add(letter5);
	    letter6 = (TextView)findViewById(R.id.actletselletter6); letters.add(letter6);
	    letter7 = (TextView)findViewById(R.id.actletselletter7); letters.add(letter7);
	    letter8 = (TextView)findViewById(R.id.actletselletter8); letters.add(letter8);
	    letter9 = (TextView)findViewById(R.id.actletselletter9); letters.add(letter9); 
	    for(int l = 0; l < 9; l++){
	    	letters.get(l).setWidth(Main.unitw*17);
	    	letters.get(l).setHeight(Main.unith*4);
	    	letters.get(l).setPadding(2, 2, 2, 2);
	    	letters.get(l).setTextSize(Main.fontSize);
	    }
	    border = (TextView)findViewById(R.id.actletselborder); border.setWidth(Main.screenw); border.setHeight(Main.unith*2); 
	    consbut = (Button)findViewById(R.id.actletselcons); consbut.setWidth(Main.unitw*68); consbut.setHeight(Main.unith*8); consbut.setTextSize(Main.fontSize);
	    vowelbut = (Button)findViewById(R.id.actletselvowel); vowelbut.setWidth(Main.unitw*68); vowelbut.setHeight(Main.unith*8); vowelbut.setTextSize(Main.fontSize);
	    
	    runnable = new Runnable(){
            @Override
            public void run() {
            	timeout.postDelayed(this,1000);	
            	//letters.get(letcount).setText("" + secondsleft);
            	secondsleft--;
            	if(secondsleft == -1){
		        	 int randnum = rand.nextInt(2);
		        	 if (randnum == 0){
		        		 if (conscount == 6){
		        			 onVowelPressed(null);
		        		 }else{
		        			 onConsPressed(null);
		        		 }
		        	 }else{
		        		 if (vowelcount == 5){
		        			 onConsPressed(null);
		        		 }else{
		        			 onVowelPressed(null);
		        		 }
		        	 }
		         }
            }
	    };
	
	    timeout.postDelayed(runnable, 500);
	}

	public void onConsPressed(View view){
		if(myletters.size() < 9){
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		int randnum = rand.nextInt(consonants.length()); //select random number
    	String letter = "" + consonants.charAt(randnum); //get the letter at this random index
    	consonants = consonants.replaceFirst(letter, "");//remove this letter from vowels string
    	myletters.add(letter);							 //add this letter to myletters ArrayList
    	letters.get(letcount).setText("" + letter);		 //Update UI...
    	letters.get(letcount).setBackgroundResource(R.drawable.smalltile);
    	letters.get(letcount).setPadding(2, 2, 2, 2);
    	conscount++;
    	if(conscount == 6){
    		consbut.setEnabled(false);
    		consbut.setBackgroundResource(R.drawable.paperbtngreyed);
    		consbut.setText("Max 6 Consonants");
    		consbut.setPadding(1, 1, 1, 1);
    	}
    	if(myletters.size() == 9){
    		nextRound();
		};
        secondsleft = 5;
        letcount++;    	};
	}
	
	public void onVowelPressed(View view){
		if(myletters.size() < 9){
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		int randnum = rand.nextInt(vowels.length());	//select random index
    	String letter = "" + vowels.charAt(randnum);	//get the letter at this random index
    	vowels = vowels.replaceFirst(letter, "");		//remove this letter from vowels string
    	if(myletters.size() < 9){
    	myletters.add(letter);							//add this letter to myletters ArrayList
    	};
    	letters.get(letcount).setText("" + letter);		//Update UI...
    	letters.get(letcount).setBackgroundResource(R.drawable.smalltile);
    	letters.get(letcount).setPadding(2, 2, 2, 2);
    	vowelcount++;
    	if(vowelcount == 5){
    		vowelbut.setEnabled(false);
    		vowelbut.setBackgroundResource(R.drawable.paperbtngreyed);
    		vowelbut.setText("Max 5 Vowels");
    		vowelbut.setPadding(1, 1, 1, 1);
    	}
    	if(myletters.size() == 9){
    		nextRound();
		};
        secondsleft = 5;
        letcount++;};
	}
	
	public void onPause(){
		super.onPause();
		secondsleft+=2;
		timeout.removeCallbacks(runnable);
	}
	
	public void onResume(){
		super.onResume();
		if(screenOn == true){
			timeout.post(runnable);
		}
	}

	public void onBackPressed() {
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish();
	        		unregisterReceiver(br);
	            	timeout.removeCallbacksAndMessages(null);
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	        					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	public void nextRound(){
		unregisterReceiver(br);
		timeout.removeCallbacksAndMessages(null);
		Intent i = new Intent(this, lettersCreate.class);
		i.putExtra("gamemode", gamemode);
    	i.putExtra("points", points);
    	i.putExtra("gameround", gameround);    	
		i.putStringArrayListExtra("myletters", myletters);
    	startActivity(i);   	
		finish();		
	}

}
