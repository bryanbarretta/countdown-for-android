package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.bryan.barrett.countdownforandroid.R;

public class lettersSubmit extends Activity {

	int points, gameround;
	String gamemode, finalword;
	ArrayList<String> myletters;
	ArrayList<String> mywordlist = new ArrayList<String>();
	static ArrayList<String> bestwords = new ArrayList<String>();
	
	TextView header;
	Button l1, l2, l3, l4, l5, l6, l7, l8, l9;
	SoundPool soundPool;
	AudioManager audio;
	boolean sound;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_letters_submit);

	    //Shared
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //intent
	    Intent i = getIntent();
	    gamemode = i.getStringExtra("gamemode");
	    points = i.getIntExtra("points", 0);
	    gameround = i.getIntExtra("gameround", 0);
	    myletters = i.getStringArrayListExtra("myletters");
	    mywordlist = i.getStringArrayListExtra("mywordlist");
	    bestwords = i.getStringArrayListExtra("bestwords");
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	   
	    header = (TextView)findViewById(R.id.actletsubheader); header.setWidth(Main.screenw); header.setHeight(Main.unith*5); header.setTypeface(Main.scribble); header.setTextSize(Main.fontSize+8);
	    l1 = (Button)findViewById(R.id.actletsubbtn1); l1.setWidth(Main.unitw*17); l1.setHeight(Main.unith*4); l1.setText(myletters.get(0)); l1.setPadding(1, 1, 1, 1); l1.setTextSize(Main.fontSize);
	    l2 = (Button)findViewById(R.id.actletsubbtn2); l2.setWidth(Main.unitw*17); l2.setHeight(Main.unith*4); l2.setText(myletters.get(1)); l2.setPadding(1, 1, 1, 1); l2.setTextSize(Main.fontSize);
	    l3 = (Button)findViewById(R.id.actletsubbtn3); l3.setWidth(Main.unitw*17); l3.setHeight(Main.unith*4); l3.setText(myletters.get(2)); l3.setPadding(1, 1, 1, 1); l3.setTextSize(Main.fontSize);
	    l4 = (Button)findViewById(R.id.actletsubbtn4); l4.setWidth(Main.unitw*17); l4.setHeight(Main.unith*4); l4.setText(myletters.get(3)); l4.setPadding(1, 1, 1, 1); l4.setTextSize(Main.fontSize);
	    l5 = (Button)findViewById(R.id.actletsubbtn5); l5.setWidth(Main.unitw*17); l5.setHeight(Main.unith*4); l5.setText(myletters.get(4)); l5.setPadding(1, 1, 1, 1); l5.setTextSize(Main.fontSize);
	    l6 = (Button)findViewById(R.id.actletsubbtn6); l6.setWidth(Main.unitw*17); l6.setHeight(Main.unith*4); l6.setText(myletters.get(5)); l6.setPadding(1, 1, 1, 1); l6.setTextSize(Main.fontSize);
	    l7 = (Button)findViewById(R.id.actletsubbtn7); l7.setWidth(Main.unitw*17); l7.setHeight(Main.unith*4); l7.setText(myletters.get(6)); l7.setPadding(1, 1, 1, 1); l7.setTextSize(Main.fontSize);
	    l8 = (Button)findViewById(R.id.actletsubbtn8); l8.setWidth(Main.unitw*17); l8.setHeight(Main.unith*4); l8.setText(myletters.get(7)); l8.setPadding(1, 1, 1, 1); l8.setTextSize(Main.fontSize);
	    l9 = (Button)findViewById(R.id.actletsubbtn9); l9.setWidth(Main.unitw*17); l9.setHeight(Main.unith*4); l9.setText(myletters.get(8)); l9.setPadding(1, 1, 1, 1); l9.setTextSize(Main.fontSize);
	    
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	    android.R.layout.simple_list_item_2, android.R.id.text1, mywordlist);
	    
	    ListView lv = (ListView)findViewById(R.id.LSlistview);
	    lv.setAdapter(adapter); 
	    lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) {
				finalword = mywordlist.get(position); nextRound();
				if(sound){
					soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
						audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
				}
			}
		});

	}
	
	private void nextRound() {
		Intent intent = new Intent(this, lettersPoints.class);
		intent.putExtra("finalword", finalword);
    	intent.putExtra("points", points);
		intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);
    	intent.putStringArrayListExtra("bestwords", bestwords);
		intent.putStringArrayListExtra("myletters", myletters);
    	startActivity(intent);
   	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	finish();
	}
	
	/*public void onPause(){
		super.onPause();
		finish();
	}*/
	
	public void onBackPressed() {
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish();
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
								audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Practice Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}


}
