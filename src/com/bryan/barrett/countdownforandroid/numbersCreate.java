package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;
import java.util.Random;
import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class numbersCreate extends Activity {

	int points, gameround;
	boolean timerInProgress;	//activates after red button is pressed
	Runnable runnable;			//timer
	ArrayList<Integer> mynumbers = new ArrayList<Integer>();
	ArrayList<Integer> mynumbersCOPY = new ArrayList<Integer>();
	ArrayList<String> memory = new ArrayList<String>();
	
	int counter = 33, targetnum = 0, currentBest, bonustimeleft = 0;
	int sum1 = 0;			//first number in an operation
	int sum2 = 0;			//second number in an operation
	String sign = null;		//sign in an operation
	int total = 0;			//result of an operation

	String gamemode;
		
	Handler h=new Handler();		//timer 30 seconds
	Handler h2=new Handler();		//target number generation animation
	 Button n1, n2, n3, n4, n5, n6, add, subtract, multiply, divide, redbutton;
	 LinearLayout undo, clear;
	 ArrayList<Button> numbers = new ArrayList<Button>();
	 TextView line1, line2, line3, line4, line5, line6, numsborder, timertext, targettext;
	
	ProgressTable progress;
	SharedPreferences prefs;
	SoundPool soundPool;
	AudioManager audio;
	boolean sound;
	
	BroadcastReceiver br;
	boolean screenOn = true;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_numbers_create);
	    
	    memory.clear();
	    
		bonustimeleft = 0; currentBest = 0;
		timerInProgress = false;
		
		//set up a BROADCAST RECEIVER and register it
	    br = new BroadcastReceiver(){
	    	@Override
	        public void onReceive(Context context, Intent intent) {
	    		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
		            //What to do if screen is turned off
	    			screenOn = false;}
		        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
		        	//What to do if user is present at app
		        	screenOn = true;
		        	onResume();
		        }
	        }
	    };
	    IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
	    filter.addAction(Intent.ACTION_SCREEN_OFF);
	    registerReceiver(br, filter);
		
		//Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //Load mynumbers from previous activity
	    Intent i = getIntent();
	    mynumbers = i.getIntegerArrayListExtra("mynumbers");
	    gamemode = i.getStringExtra("gamemode");
	    gameround = i.getIntExtra("gameround", 0);
	    points = i.getIntExtra("points", 0);
	    
	    //add operators to sign arraylist and add duplicate values to mynumbersCOPY
		for(int x=0; x<=5; x++){
			mynumbersCOPY.add(mynumbers.get(x));
		}
	    
	    //mynumbers
	    n1 = (Button)findViewById(R.id.n1); numbers.add(n1);
	    n2 = (Button)findViewById(R.id.n2); numbers.add(n2);
	    n3 = (Button)findViewById(R.id.n3); numbers.add(n3);
	    n4 = (Button)findViewById(R.id.n4); numbers.add(n4);
	    n5 = (Button)findViewById(R.id.n5); numbers.add(n5);
	    n6 = (Button)findViewById(R.id.n6); numbers.add(n6);
	    
	    for (int n = 0; n < 6; n++){
	    	numbers.get(n).setWidth(Main.screenw/6); numbers.get(n).setHeight(Main.unith*5); 
	    	numbers.get(n).setText("" + mynumbers.get(n)); numbers.get(n).setTextSize(Main.fontSize);
	    	numbers.get(n).setEnabled(false); numbers.get(n).setPadding(1, 1, 1, 1);
	    }
	    
	    
	    //TargetNumber Animation (h2) and Timer Animation (h)
	    Typeface led = Typeface.createFromAsset(getAssets(), "fonts/led.ttf");
	    timertext = (TextView)findViewById(R.id.timertext); timertext.setTypeface(led); 
	    timertext.setHeight(Main.unith*4); timertext.setTextSize(Main.fontSize+20);
	    targettext = (TextView)findViewById(R.id.targettext); targettext.setTypeface(led);
	    targettext.setTextSize(Main.fontSize+20);
	    redbutton = (Button)findViewById(R.id.redbutton);
	    
	    //command buttons
	    clear = (LinearLayout)findViewById(R.id.clearbut); clear.setEnabled(false); 
	    	android.view.ViewGroup.LayoutParams params = clear.getLayoutParams(); params.height = ((int) (Main.unith*5.5));
	    	TextView clrTXT = (TextView)findViewById(R.id.clearbutTXT); clrTXT.setTextSize(Main.fontSize-10);//clear.setTextSize(fontSize-10);
	    undo = (LinearLayout)findViewById(R.id.undobut); undo.setEnabled(false); //undo.setTextSize(fontSize-10);
	    	TextView undTXT = (TextView)findViewById(R.id.undobutTXT); undTXT.setTextSize(Main.fontSize-10);
	    	
	    
	    //operators (initially disabled)
	    subtract = (Button)findViewById(R.id.subtractbut); subtract.setEnabled(false); subtract.setTextSize(Main.fontSize+10); 
    	add = (Button)findViewById(R.id.addbut); add.setEnabled(false); add.setTextSize(Main.fontSize+10);
    	multiply = (Button)findViewById(R.id.multiplybut); multiply.setEnabled(false); multiply.setTextSize(Main.fontSize+10);
    	divide = (Button)findViewById(R.id.dividebut); divide.setEnabled(false); divide.setTextSize(Main.fontSize+10);
    	
    	
    	//log + myAnswers
    	line1 = (TextView)findViewById(R.id.line1); line1.setTypeface(Main.scribble); line1.setTextSize(Main.fontSize-8);
    	line2 = (TextView)findViewById(R.id.line2); line2.setTypeface(Main.scribble); line2.setTextSize(Main.fontSize-8);
    	line3 = (TextView)findViewById(R.id.line3); line3.setTypeface(Main.scribble); line3.setTextSize(Main.fontSize-8);
    	line4 = (TextView)findViewById(R.id.line4); line4.setTypeface(Main.scribble); line4.setTextSize(Main.fontSize-8);
    	line5 = (TextView)findViewById(R.id.line5); line5.setTypeface(Main.scribble); line5.setTextSize(Main.fontSize-8);
    	line6 = (TextView)findViewById(R.id.line6); line6.setPadding(1, 1, 1, 1); 
    	line6.setGravity(Gravity.CENTER_VERTICAL); line6.setTextSize(Main.fontSize-8);
    	
    	runnable = new Runnable(){
            @Override
            public void run() {
                h.postDelayed(this,1000);
                if(counter == 33){
                	timertext.setTextColor(Color.RED);
                	timertext.setText("Ready?");    
                	line1.setText(" ");
                }else if(counter == 32){
                	timertext.setTextColor(Color.rgb(255, 150, 0));
                	timertext.setText("Set!");
                	if(sound){
                		soundPool.play(Main.SNDbeep1, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
                			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                	}
                }else if(counter == 31){
                	h2.removeCallbacksAndMessages(null);
                	n1.setEnabled(true);n2.setEnabled(true);n3.setEnabled(true);n4.setEnabled(true);n5.setEnabled(true);n6.setEnabled(true);
                	undo.setEnabled(true); clear.setEnabled(true);
                	timertext.setTextColor(Color.GREEN);
                	timertext.setText("GO!!");
                	if(sound){
                		soundPool.play(Main.SNDbeep2, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
                			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                	}
                }else if(counter == -1){
                	timertext.setText("TIME UP");            
                	n1.setEnabled(false);n2.setEnabled(false);n3.setEnabled(false);
                	n4.setEnabled(false);n5.setEnabled(false);n6.setEnabled(false);
                	undo.setEnabled(false); clear.setEnabled(false); subtract.setEnabled(false);
                	add.setEnabled(false); multiply.setEnabled(false); divide.setEnabled(false);
                }
                else if(counter == -2){
                	//timeup               	
                	nextRound();	
                }else{
                	timertext.setTextColor(Color.RED);
                	if(sound){
                		if(counter <= 30 && counter > 5){
                			soundPool.play(Main.SNDtick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
                					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                		}else if(counter <= 5 && counter > 0){
                    		soundPool.play(Main.SNDtickFinal, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
                    				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                    	}
                		else if(counter == 0){
                    		soundPool.play(Main.SNDtimeup, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
                    				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                    	}
                	}
                	if(counter>9){
                	timertext.setText("00:" + counter);
                	}else{
                		timertext.setText("00:0" + counter);	
                	}
                }                
                counter--;
            }
        };
	}
	
	public void onPause(){
		super.onPause();
			if(timerInProgress == true){
				h.removeCallbacks(runnable);
			}
		}
	
	public void onResume(){
		super.onResume();
		if(screenOn == true && timerInProgress == true){
				h.post(runnable);
		}
	}
	
	public void redButtonPressed(View view){
		
		counter = 33; 
		redbutton.setClickable(false);
		redbutton.setBackgroundResource(R.drawable.redbuttongreyed);
		if(sound){
			soundPool.play(Main.SNDbeep1, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		
		h2.post(new Runnable(){
            @Override
            public void run() {
                h2.postDelayed(this,10);
        	    //Generate a target number
        	    final Random rand = new Random();
        		targetnum = rand.nextInt(999);
        		if(targetnum<=100){
        		targetnum = targetnum + 101;	
        		}
        		targettext.setText("" + targetnum);
            }
    	});
		
		timerInProgress = true;
    	
		h.post(runnable);
	}
	
	public void addPressed(View view){
		signPressed("+", 0);
		add.setBackgroundResource(R.drawable.smalltilegrey); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
		multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
		subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
	}
	public void subtractPressed(View view){
		signPressed("-", 1);
		add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.smalltilegrey);
		multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
		add.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
	}
	public void multiplyPressed(View view){
		signPressed("�", 2);
		add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
		multiply.setBackgroundResource(R.drawable.smalltilegrey); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
		subtract.setPadding(1, 1, 1, 1); add.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
	}
	public void dividePressed(View view){
		signPressed("�", 3);
		add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
		multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.smalltilegrey);
		subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); add.setPadding(1, 1, 1, 1);
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
	} 
	
	public void n1pressed(View view){
		String btnval = Integer.toString(mynumbersCOPY.get(0));
    	int btnID = 0;
    	numPressed(n1, btnval, btnID);
    	if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	public void n2pressed(View view){
		String btnval = Integer.toString(mynumbersCOPY.get(1));
    	int btnID = 1;
    	numPressed(n2, btnval, btnID);
    	if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	public void n3pressed(View view){
		String btnval = Integer.toString(mynumbersCOPY.get(2));
    	int btnID = 2;
    	numPressed(n3, btnval, btnID);
    	if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	public void n4pressed(View view){
		String btnval = Integer.toString(mynumbersCOPY.get(3));
    	int btnID = 3;
    	numPressed(n4, btnval, btnID);
    	if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	public void n5pressed(View view){
		String btnval = Integer.toString(mynumbersCOPY.get(4));
    	int btnID = 4;
    	numPressed(n5, btnval, btnID);
    	if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	public void n6pressed(View view){
		String btnval = Integer.toString(mynumbersCOPY.get(5));
    	int btnID = 5;
    	numPressed(n6, btnval, btnID);
    	if(sound){
    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	
	public  void numPressed(Button btn, String buttonValue, int buttonID){
		
		if(sum1 == 0){							/**USER IS SELECTING THE FIRST NUMBER IN THE EQUATION**/
			
			btn.setEnabled(false);
			btn.setBackgroundResource(R.drawable.smalltilegrey);
			btn.setPadding(1, 1, 1, 1);
    		sum1 = Integer.parseInt(buttonValue);
    		add2memory(buttonValue, buttonID); //adds button value and ID to memory (needed for undo)
    		getLine().setText(" " + sum1);
    		add.setEnabled(true);subtract.setEnabled(true);multiply.setEnabled(true);divide.setEnabled(true);
    		add.setBackgroundResource(R.drawable.yellowbuttononclick);subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
    		multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
    		add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
    	}
		else if(sum2 == 0 && sign == null){ 	/**USER IS REPLACING THE FIRST PART OF EQUATION WITH DIFFERENT NUMBER**/
    		
			numbers.get(Integer.parseInt(memory.get(memory.size()-1))).setEnabled(true);	  
    		numbers.get(Integer.parseInt(memory.get(memory.size()-1))).setBackgroundResource(R.drawable.smalltileonclick);
    		numbers.get(Integer.parseInt(memory.get(memory.size()-1))).setPadding(1, 1, 1, 1);
    		memory.remove(memory.size()-1); memory.remove(memory.size()-1);
    		
    		btn.setEnabled(false);
			btn.setBackgroundResource(R.drawable.smalltilegrey);
			btn.setPadding(1, 1, 1, 1);
    		sum1 = Integer.parseInt(buttonValue);
    		add2memory(buttonValue, buttonID); //adds button value and ID to memory (needed for undo)
    		getLine().setText(" " + sum1);
    	}
    	else{									/**USER IS SELECTING THE SECOND NUMBER IN THE EQUATION**/			
    		sum2 = Integer.parseInt(buttonValue);
    		total = getTotal();
    		isAnswerBetter(total, currentBest);
    		
    		if(total == targetnum){			//USER HAS REACHED THE TARGET NUMBER WITH TIME TO SPARE
    			currentBest = total;
    			bonustimeleft = counter;
    			nextRound();
    		}else
    			if(total == -1){			//USER HAS MADE AN INVALID EQUATION: i.e... 5 - 50... 6 � 10
    				return;
    			}else{						//USER HAS FINISHED AN EQUATION
    				add2memory(buttonValue, buttonID);
    				mynumbersCOPY.set(buttonID, total);
    				btn.setText("" + total);
    				btn.setBackgroundResource(R.drawable.smalltilepurpleonclick);
    				btn.setPadding(1, 1, 1, 1);
    				sign = null;
    				sum1 = sum2 = 0;
    				add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
    				multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
    				add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
        	}
    	}
	}

	public  void signPressed(String signValue, int signID){
        	sign = signValue;
        	String lbp = memory.get(memory.size()-2);	//last button pressed
        	String llbp = null;
        	
        	if(memory.size()>4){ 		
        		/**this is for if the user hits a sign, and the second last button pressed was a sign
        		 * e.g. user hit: num, sign, num, sign **/
        		llbp = memory.get(memory.size()-4);		//second last button pressed
        		if(llbp.equals("+")||llbp.equals("-")||llbp.equals("�")||llbp.equals("�")){
            		sum1 = total;	 //mimic as if the user hit had started a new equation with the answer of the last one
            		numbers.get(Integer.parseInt(memory.get(memory.size()-1))).setEnabled(false);
            		numbers.get(Integer.parseInt(memory.get(memory.size()-1))).setBackgroundResource(R.drawable.smalltilegrey);
            		numbers.get(Integer.parseInt(memory.get(memory.size()-1))).setPadding(1, 1, 1, 1);
            		add2memory(String.valueOf(total), Integer.parseInt(memory.get(memory.size()-1)));
            	}
        	}
        		
        	if(lbp.equals("+")||lbp.equals("-")||lbp.equals("�")||lbp.equals("�")){
        		/**this is for if the user hits a sign, and then hits a different sign
        		 * e.g. user hit: num, sign, sign **/
        		memory.remove(memory.size()-1); memory.remove(memory.size()-1);
        	}
        	
        	
    		getLine().setText(" " + sum1 + " " + signValue);	
    		add2memory(signValue, signID);					//adds that sign to memory
    		add.setEnabled(true);subtract.setEnabled(true);multiply.setEnabled(true);divide.setEnabled(true);
    }
	
	public  void resetpressed(View view){
		if(sound){
        	soundPool.play(Main.SNDshuffle, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
        			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		getLine().setText(" ");
    	mynumbersCOPY.clear();
    	memory.clear();
    	sum1 = sum2 = total = 0; 
    	sign = null;
    	for(int i=0; i<6; i++){
			mynumbersCOPY.add(mynumbers.get(i));
		}       
		n1.setText("" + mynumbersCOPY.get(0)); n1.setEnabled(true); n1.setBackgroundResource(R.drawable.smalltileonclick); n1.setPadding(1, 1, 1, 1);
		n2.setText("" + mynumbersCOPY.get(1)); n2.setEnabled(true); n2.setBackgroundResource(R.drawable.smalltileonclick); n2.setPadding(1, 1, 1, 1);
		n3.setText("" + mynumbersCOPY.get(2)); n3.setEnabled(true); n3.setBackgroundResource(R.drawable.smalltileonclick); n3.setPadding(1, 1, 1, 1);
		n4.setText("" + mynumbersCOPY.get(3)); n4.setEnabled(true); n4.setBackgroundResource(R.drawable.smalltileonclick); n4.setPadding(1, 1, 1, 1);
		n5.setText("" + mynumbersCOPY.get(4)); n5.setEnabled(true); n5.setBackgroundResource(R.drawable.smalltileonclick); n5.setPadding(1, 1, 1, 1);
		n6.setText("" + mynumbersCOPY.get(5)); n6.setEnabled(true); n6.setBackgroundResource(R.drawable.smalltileonclick); n6.setPadding(1, 1, 1, 1);
    	line1.setText(" ");line2.setText(" ");line3.setText(" ");line4.setText(" ");line5.setText(" ");
    	add.setEnabled(false);subtract.setEnabled(false);multiply.setEnabled(false);divide.setEnabled(false);
		add.setBackgroundResource(R.drawable.smalltilegrey); subtract.setBackgroundResource(R.drawable.smalltilegrey);
		multiply.setBackgroundResource(R.drawable.smalltilegrey); divide.setBackgroundResource(R.drawable.smalltilegrey);
	}
	
	public void undopressed(View view)
    {
		if(memory.isEmpty()){
			getLine().append("");
			return;
		}
		else{
			if(sound){
				soundPool.play(Main.SNDundo, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
			}
			String lbID = memory.get(memory.size()-1);
			String lbVal = memory.get(memory.size()-2);
			if(lbID.equals("0")){
				undo(n1, lbID, lbVal);
			}else if(lbID.equals("1")){
				undo(n2, lbID, lbVal);
			}else if(lbID.equals("2")){
				undo(n3, lbID, lbVal);
			}else if(lbID.equals("3")){
				undo(n4, lbID, lbVal);
			}else if(lbID.equals("4")){
				undo(n5, lbID, lbVal);
			}else if(lbID.equals("5")){
				undo(n6, lbID, lbVal);
			}
		}
    }
	//UNDO LISTENER
	public  void undo(Button btn, String lbID, String lbVal){
			/**undoing a sign button: 		delete sign and leave first number in operation**/
			if(lbVal.equals("+")||lbVal.equals("-")||lbVal.equals("�")||lbVal.equals("�")){
				add.setEnabled(true);subtract.setEnabled(true);multiply.setEnabled(true);divide.setEnabled(true);
		    	memory.remove(memory.size()-1); memory.remove(memory.size()-1);
		    	getLine().setText(" " + sum1);
		    	sign = null;    
		    	add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
				multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
				add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
			}
			/**undoing a number button:		must identify what part of operation the number is**/
			else{
				/**SCENARIO 1: number is 1st number button pressed (i.e. preceded by nothing)**/
				if(memory.get(1)==lbID){
					btn.setEnabled(true);
					btn.setBackgroundResource(R.drawable.smalltileonclick);
					btn.setPadding(1, 1, 1, 1);
					add.setEnabled(false);subtract.setEnabled(false);multiply.setEnabled(false);divide.setEnabled(false);
					sum1 = 0;		//num undone was first num in an operation; sum1 is reset to 0
					getLine().setText(" ");
					memory.remove(memory.size()-1); memory.remove(memory.size()-1);
			    	System.out.println("Memory: " + memory);System.out.println("Array: " + mynumbersCOPY);
				}
				else{
					String prevVal = memory.get(memory.size()-4); //Value of 2nd last button pressed
					
					/**SCENARIO 2: number is 2nd part of operation (i.e. preceded by a sign)**/
					if(prevVal.equals("+")||prevVal.equals("-")||prevVal.equals("�")||prevVal.equals("�")){
						//first, determine which was the last sign pressed and grey it out
						if(prevVal.equals("+")){
							add.setBackgroundResource(R.drawable.smalltilegrey); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
							multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
							add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
						}else if(prevVal.equals("-")){
							add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.smalltilegrey);
							multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
							add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
					    }else if(prevVal.equals("�")){
							add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
							multiply.setBackgroundResource(R.drawable.smalltilegrey); divide.setBackgroundResource(R.drawable.yellowbuttononclick);
							add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
					    }else if(prevVal.equals("�")){
							add.setBackgroundResource(R.drawable.yellowbuttononclick); subtract.setBackgroundResource(R.drawable.yellowbuttononclick);
							multiply.setBackgroundResource(R.drawable.yellowbuttononclick); divide.setBackgroundResource(R.drawable.smalltilegrey);
							add.setPadding(1, 1, 1, 1); subtract.setPadding(1, 1, 1, 1); multiply.setPadding(1, 1, 1, 1); divide.setPadding(1, 1, 1, 1);
					    }
						sum2 = 0;			//num undone was second num in an operation; sum2 is reset to 0
						sign = memory.get(memory.size()-4);		//use memory to find sum1 and sign values
						sum1 = Integer.parseInt(memory.get(memory.size()-6));
						total = 0;
						
						int sum1temp = sum1; String signtemp = sign;	/*needed for undoing...
						text must be set after sum1 and sign are removed from memory so getTotal() can 
						calculate which line the current operation is on. If text is set before they
						are removed then the next line will display it, not the current one.*/
												
						btn.setText(lbVal);
						btn.setBackgroundResource(R.drawable.smalltileonclick);
						mynumbersCOPY.set(Integer.parseInt(lbID), Integer.parseInt(lbVal));
						memory.remove(memory.size()-1); memory.remove(memory.size()-1);
						add.setEnabled(true);subtract.setEnabled(true);multiply.setEnabled(true);divide.setEnabled(true);
						getLine().setText(" " + sum1temp + " " + signtemp);
						
						sum1temp=0; signtemp=null;
					}
					/**SCENARI0 3: number is 1st part of an operation (i.e. preceded by a number)**/
					else{
						btn.setEnabled(true);
						btn.setBackgroundResource(R.drawable.smalltileonclick);
						getLine().setText(" ");
						sum1 = sum2 = total = 0; sign = null; 
						memory.remove(memory.size()-1); memory.remove(memory.size()-1);
						add.setEnabled(false);subtract.setEnabled(false);multiply.setEnabled(false);divide.setEnabled(false);
						add.setBackgroundResource(R.drawable.smalltilegrey); subtract.setBackgroundResource(R.drawable.smalltilegrey);
						multiply.setBackgroundResource(R.drawable.smalltilegrey); divide.setBackgroundResource(R.drawable.smalltilegrey);
					}
				}
			}
		}

	public void isAnswerBetter (int answer, int currBest){
		
		int currOffset = 0;			//How much the current best answer is off by
		if(currBest > targetnum){
			currOffset = currBest - targetnum;
		}else{
			currOffset = targetnum - currBest;
		}
		
		int ansOffset = 0;			//How much this answer is off by
		if(answer > targetnum){
			ansOffset = answer - targetnum;
		}else{
			ansOffset = targetnum - answer;
		}
		
		if (ansOffset < currOffset){	//If this answer is off by less than the current best answer
			currentBest = answer;		//...this answer is the new currentBest
			line6.setText("Your Best Answer: " + currentBest);
		}else{
			return;
		}
	}
	
 	public  int getTotal(){	
		if (sign.equals("+")){
			total = sum1 + sum2;
			getLine().setText(" " + sum1 + " " + sign + " " + sum2 + " = " + total);
		}else if(sign.equals("-")){
			if((sum1-sum2)<0){		
				getLine().setText(" No negative numbers! " + sum1 + " " + sign);
				total = -1;
			}else{
				total = sum1 - sum2;
				getLine().setText(" " + sum1 + " " + sign + " " + sum2 + " = " + total);
			}
		}else if(sign.equals("�")){
			total = sum1 * sum2;
			getLine().setText(" " + sum1 + " " + sign + " " + sum2 + " = " + total);
		}else if(sign.equals("�")){
			if((sum1%sum2)==0){
				total = sum1 / sum2;
				getLine().setText(" " + sum1 + " " + sign + " " + sum2 + " = " + total);
			}else{
				getLine().setText(" No fractions allowed! " + sum1 + " " + sign);
				total = -1;
			}
		}
		return total;
	}
	
	public  TextView getLine(){
		if(memory.size()>=6 && memory.size()<=11){
			return line2;
		}else if(memory.size()>=12 && memory.size()<=17){
			return line3;
		}else if(memory.size()>=18 && memory.size()<=23){
			return line4;
		}else if(memory.size()>=24){
			return line5;
		}else{
			return line1;
		}
	}
	
	public void add2memory(String buttonValue, int buttonID){
		memory.add(buttonValue);
		memory.add("" + buttonID);
	}

	public void onBackPressed() {
		progress = new ProgressTable(this);
		
		//make this an undo button?
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish();
	            	if(sound){
	            		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	            			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	            	}
	            	if(gamemode.equals("full")){
	            		progress.add(0, mynumbers + "", "*forfeited", "" + targetnum);
	            	}
            	    progress.close();
            		unregisterReceiver(br);
	            	clearmemory();
	            }
	            case DialogInterface.BUTTON_NEGATIVE:

            	    progress.close();
	                return;
	            }
	        }
	    };
	    String question = "";
	    if(gamemode.equals("full")){
	    	question = "Are you sure you want to quit? (You will forfeit the round)";
	    }else{
	    	question = "Return to menu?";
	    }
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(question).setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	public void nextRound(){
		unregisterReceiver(br);
		Intent intent = new Intent(this, numbersSubmit.class);
		intent.putIntegerArrayListExtra("mynumbers", mynumbers);

		intent.putExtra("currentBest", currentBest);
		intent.putExtra("targetnum", targetnum);
		intent.putExtra("bonustimeleft", bonustimeleft);
		
		intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);
		intent.putExtra("points", points);
		
    	startActivity(intent);
   	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	finish();
    	clearmemory();
	}
	
	public void clearmemory(){
		h.removeCallbacksAndMessages(null);
    	mynumbers.clear(); mynumbersCOPY.clear();memory.clear();
    	counter = sum1 = sum2 = total = bonustimeleft = targetnum = 0; 
    	sign = null;
    	numbers.clear();
	}
}
