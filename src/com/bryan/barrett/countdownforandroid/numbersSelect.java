package com.bryan.barrett.countdownforandroid;
import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.bryan.barrett.countdownforandroid.R;

public class numbersSelect extends Activity {

	SharedPreferences prefs;
	final Random rand = new Random();
	final ArrayList<Integer> bignumbers = new ArrayList<Integer>();
	final ArrayList<Integer> smallnumbers = new ArrayList<Integer>();
  	final ArrayList<Integer> mynumbers = new ArrayList<Integer>();
  	Button num1, num2, num3, num4, num5, num6, lrg, sml;
  	ArrayList<Button> nums = new ArrayList<Button>();
  	int j = 0, points, gameround;
  	String gamemode;
  	SoundPool soundPool;
  	AudioManager audio;
  	boolean sound;
	
		/** Called when the activity is first created. */
		public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
		    setContentView(R.layout.activity_numbers_select);
		    
		    Intent i = getIntent();
		    gamemode = i.getStringExtra("gamemode");	
		    gameround = i.getIntExtra("gameround", 0) + 1;
		    points = i.getIntExtra("points", 0);    
		    
		    //Shared Preferences
		    prefs = PreferenceManager.getDefaultSharedPreferences(this);
		    sound = prefs.getBoolean("sound", true);
		    
		    //Sounds
		    setVolumeControlStream(AudioManager.STREAM_MUSIC);
		    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		    soundPool = Main.soundPool;
		    
		    //Big Numbers
		  	for (int n=25; n<=100; n+=25){		//add 25, 50, 75, 100
		  		bignumbers.add(n); 				
		  	}
		  	//Small Numbers
		  	for (int m=1; m<=10; m++){		//adds 1-10 twice
		  		smallnumbers.add(m); 
		  		smallnumbers.add(m); 
		  	}
		  	
		    //Screen metrics
		    DisplayMetrics metrics = new DisplayMetrics();
		    getWindowManager().getDefaultDisplay().getMetrics(metrics);
		    int screenw = metrics.widthPixels; int screenh = metrics.heightPixels;
		    int unitw = (screenw/14); int unith = (screenh/40);	//divide screen into 14 columns x 40 rows...
		  	
		    num1 = (Button)findViewById(R.id.num1img); nums.add(num1); 
		  	num2 = (Button)findViewById(R.id.num2img); nums.add(num2); 
		  	num3 = (Button)findViewById(R.id.num3img); nums.add(num3); 
		  	num4 = (Button)findViewById(R.id.num4img); nums.add(num4); 
		  	num5 = (Button)findViewById(R.id.num5img); nums.add(num5); 
		  	num6 = (Button)findViewById(R.id.num6img); nums.add(num6); 
		  	for(int n=0; n<6; n++){
		  		nums.get(n).setHeight(screenh/4); nums.get(n).setTextSize(Main.fontSize); 
		  		nums.get(n).setWidth(screenw/6); nums.get(n).setPadding(1, 1, 1, 1);
		  	}
		  	lrg = (Button)findViewById(R.id.largebutton); lrg.setWidth((unitw*200)/35); 
		  	sml = (Button)findViewById(R.id.smallbutton); sml.setWidth((unitw*200)/35);
		  	
		  	Typeface scribble = Typeface.createFromAsset(getAssets(), "fonts/scribble.ttf");
	    	
	        TextView numbersSeltext = (TextView)findViewById(R.id.numbersSeltext);
	        numbersSeltext.setTypeface(scribble);
	        numbersSeltext.setWidth(screenw); numbersSeltext.setHeight(unith*8);
	        if(gamemode.equals("full")){
	        	numbersSeltext.setText("Round " + gameround + ": Select 6 Numbers");
		    }
	}
	
	public void onBigPressed(View view){
		if(mynumbers.size()<6){
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		final int randnum = rand.nextInt(bignumbers.size());
		final int randbignum = bignumbers.get(randnum);
		mynumbers.add(randbignum);
        bignumbers.remove(randnum);       
        nums.get(j).setVisibility(View.VISIBLE);
        nums.get(j).setText("" + randbignum);
        if(bignumbers.size()==0){
        	//grey button out
        	lrg.setText("None Left");
        	lrg.setClickable(false);
        	lrg.setBackgroundResource(R.drawable.paperbtngreyed);
        }
		if(mynumbers.size() == 6){
			nextRound(mynumbers);
		};
		j++;
		};
	}
	
	public void onSmallPressed(View view){
		if(mynumbers.size()<6){
		if(sound){
			soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
		}
		final int randnum = rand.nextInt(smallnumbers.size());
		final int randsmallnum = smallnumbers.get(randnum);
		mynumbers.add(randsmallnum);
        smallnumbers.remove(randnum);
        nums.get(j).setVisibility(View.VISIBLE);
        nums.get(j).setText("" + randsmallnum);
		if(mynumbers.size() == 6){
			nextRound(mynumbers);
		};
		j++;
		}
	}

	public void onBackPressed() {
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish();
	            	if(sound){
	        			soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	        				audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	        		}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Practice Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	public void nextRound(ArrayList<Integer> mynumbers){
		
		Intent intent = new Intent(this, numbersCreate.class);
		intent.putIntegerArrayListExtra("mynumbers", mynumbers);
		intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);    	
		intent.putExtra("points", points);
    	startActivity(intent);
   	
    	finish();
		//do next round
	}
	
}
