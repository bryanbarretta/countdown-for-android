/* Solve function : Copyright (C) 2002 J. M. Spivey ... http://www.cs.ox.ac.uk/admissions/ugrad/CountDown */

package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;
import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class numbersSubmit extends Activity {
	
    private String solution;
    private TextView solutionview, targetview, n1, n2, n3, n4, n5, n6, usertext, pointsView,
    	levelView, toNextLevelView;
    private Button button1, button2;
    private String gamemode;
    private ProgressBar progressbar;
    int points, pointsnew=0, pointscounter, bonustimeleft=0, gameround, pointslimit, useranswer;
    private Handler updatepoints=new Handler();
	long lvl;
	int insideAns = 0;
	Toast levelUpToast;

    ProgressTable progress;
	SharedPreferences prefs;
	
	SoundPool soundPool;
	AudioManager audio;
	boolean sound;
    
	//Used in Solve function
	private static ArrayList<Integer> mynumbers = new ArrayList<Integer>();
	private static int targetnum;
	private static int N = 6;
    private static int expN = 1 << N;
    private static int HSIZE = 20000;
    
    private static int CONST = 0, PLUS = 1, MINUS = 2, TIMES = 3, DIVIDE = 4;
    
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_numbers_submit);
	    
	    //Load mynumbers from previous activity
	    Intent i = getIntent();
	    mynumbers = i.getIntegerArrayListExtra("mynumbers");
	    targetnum = i.getIntExtra("targetnum", 0);
	    points = i.getIntExtra("points", 0);
	    gameround = i.getIntExtra("gameround", 0);
	    useranswer = i.getIntExtra("currentBest", 0);
	    pointscounter = points % 100;
	    gamemode = i.getStringExtra("gamemode");
	    bonustimeleft = i.getIntExtra("bonustimeleft", 0); if(bonustimeleft<0){bonustimeleft=0;};
	    
	    solution = Solve(mynumbers, targetnum);
	    
	    //db
	    progress = new ProgressTable(this);
	    
	    //Shared Preferences
	    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    lvl = prefs.getLong("userlvl", 1);
	    pointslimit = (int) (lvl*10 + 100);
	    pointscounter = (int) prefs.getLong("pointscounter", 0);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
	    //Toast Message
	    levelUpToast = Toast.makeText(this, "LEVEL UP!", Toast.LENGTH_SHORT);
	    levelUpToast.setGravity(Gravity.BOTTOM|Gravity.LEFT, Main.screenw/5, (int) (Main.screenh/3.3));
	    
	    //RED BG IF INCORRECT
	    TextView BG = (TextView)findViewById(R.id.actnumsubLEFTtext2C);
	    if(getpoints() == 0){
	    	BG.setBackgroundResource(R.drawable.lightredbutton);
	    	if(sound){
	    		soundPool.play(Main.SNDwrong, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    	}
	    }else{
	    	if(sound){
	    		soundPool.play(Main.SNDsuccess, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    	}
	    }
	    
	    //2 BUTTONS
	    button1 = (Button)findViewById(R.id.actnumsubbutton1); button1.setWidth(Main.unitw*60); button1.setHeight(Main.unith*4); button1.setEnabled(false); button1.setTextSize(Main.fontSize);
	    button2 = (Button)findViewById(R.id.actnumsubbutton2); button2.setWidth(Main.unitw*60); button2.setHeight(Main.unith*4); button2.setEnabled(false); button2.setTextSize(Main.fontSize);
	    if(gamemode.equals("practice")){
	    	button1.setText("Back to menu");
	    	button2.setText("Play Again");
	    }else if(gamemode.equals("quick")){
	    	button1.setText("Quit");
	    	button2.setText("Conundrum Round");
	    }else if(gamemode.equals("full")){
	    	if(gameround == 5 || gameround == 10){
		    	button1.setText("Save & Quit");
		    	button2.setText("Progress Report");	    		
	    	}else{
		    	button1.setText("Save & Quit");
		    	button2.setText("Round " + (gameround+1) + ": Conundrum");	 
	    	}
	    }
	    
	    //SOLUTION VIEW
	    solutionview = (TextView)findViewById(R.id.actnumsubsolution); solutionview.setText(solution); 
	    	solutionview.setTypeface(Main.scribble); solutionview.setTextSize(Main.fontSize-10); solutionview.setLineSpacing(1, (float) 0.8);

	    //TARGET VIEW
	    targetview = (TextView)findViewById(R.id.actnumsubtarget); targetview.setText("" + targetnum);
	    	targetview.setTypeface(Main.led); targetview.setTextColor(Color.GREEN); targetview.setTextSize(Main.fontSize+15);
	    	
	    //6 NUMBERS
	    n1 = (TextView)findViewById(R.id.actnumsubn1); n1.setWidth(Main.unitw*22); n1.setHeight(Main.unith*5); n1.setText("" + mynumbers.get(0)); n1.setTextSize(Main.fontSize); n1.setPadding(1, 1, 1, 1);
	    n2 = (TextView)findViewById(R.id.actnumsubn2); n2.setWidth(Main.unitw*22); n2.setHeight(Main.unith*5); n2.setText("" + mynumbers.get(1)); n2.setTextSize(Main.fontSize); n2.setPadding(1, 1, 1, 1);
	    n3 = (TextView)findViewById(R.id.actnumsubn3); n3.setWidth(Main.unitw*22); n3.setHeight(Main.unith*5); n3.setText("" + mynumbers.get(2)); n3.setTextSize(Main.fontSize); n3.setPadding(1, 1, 1, 1);
	    n4 = (TextView)findViewById(R.id.actnumsubn4); n4.setWidth(Main.unitw*22); n4.setHeight(Main.unith*5); n4.setText("" + mynumbers.get(3)); n4.setTextSize(Main.fontSize); n4.setPadding(1, 1, 1, 1);
	    n5 = (TextView)findViewById(R.id.actnumsubn5); n5.setWidth(Main.unitw*22); n5.setHeight(Main.unith*5); n5.setText("" + mynumbers.get(4)); n5.setTextSize(Main.fontSize); n5.setPadding(1, 1, 1, 1);
	    n6 = (TextView)findViewById(R.id.actnumsubn6); n6.setWidth(Main.unitw*22); n6.setHeight(Main.unith*5); n6.setText("" + mynumbers.get(5)); n6.setTextSize(Main.fontSize); n6.setPadding(1, 1, 1, 1);
	    usertext = (TextView)findViewById(R.id.actnumsubtext); usertext.setText(defineusertext()); usertext.setTypeface(Main.scribble); usertext.setTextSize(Main.fontSize-10); usertext.setLineSpacing(1, (float) 0.8);
	    
	   	if(gamemode.equals("full")){
	    	progress.add(getpoints(), mynumbers + "","" + useranswer, "" + targetnum);
	    }
	    progress.close();
	    
	   	//LEVEL / TONEXTLEVEL VIEWS
	    levelView = (TextView)findViewById(R.id.actnumsubLevelText); levelView.setText("Level: " + lvl); levelView.setTextSize(Main.fontSize-13);
	    toNextLevelView = (TextView)findViewById(R.id.actnumsubToNextLevel); 
	    	toNextLevelView.setText(pointscounter + "/" + pointslimit); toNextLevelView.setTextSize(Main.fontSize-13);
	    
	    	
	    //POINTS TEXT (UPDATED IN PROGRESS BAR ANIMATION)
	    TextView pointsViewA = (TextView)findViewById(R.id.actnumsubLEFTtext2A); pointsViewA.setTextSize(Main.fontSize-13);
		pointsView = (TextView)findViewById(R.id.actnumsubLEFTtext2B); pointsView.setTextSize(Main.fontSize-13);
		final int originalPoints = points;
		final int plusPoints = getpoints()-bonustimeleft;
		pointsView.setText(originalPoints + "\n" + plusPoints + "\n" + bonustimeleft + "\n" + points);
		if(gamemode.equals("practice")){
			pointsView.setVisibility(View.INVISIBLE);
			findViewById(R.id.actnumsubLEFTtext2A).setVisibility(View.INVISIBLE);
		}
	    
		//PROGRESS BAR ANIMATION
	   	if(getpoints()==pointslimit){
	   		pointscounter++;
	   		points+=1;
	   	}
	   	final int newTotal = points + getpoints();
	   	
	   	//score = (TextView)findViewById(R.id.actnumsubscore); score.setText("Score: " + points);
	   	progressbar = (ProgressBar)findViewById(R.id.actnumsubprogressBar); progressbar.setMax(pointslimit);
	   	progressbar.setProgress(pointscounter);
	   	
	   	if(useranswer == 0 || gamemode.equals("practice")){	//...Skip the progress bar animation
	    	button1.setEnabled(true); button1.setBackgroundResource(R.drawable.paperbtnpressed); button1.setPadding(1, 1, 1, 1);
       		button2.setEnabled(true); button2.setBackgroundResource(R.drawable.paperbtnpressed); button2.setPadding(1, 1, 1, 1);
       		if(gamemode.equals("practice")){
       			BG.setVisibility(View.INVISIBLE);
       		}
	   	}else{													//...Run the progress bar animation
	    	updatepoints.postDelayed(new Runnable(){			
	    		@Override
	    		public void run() {
	    			updatepoints.postDelayed(this,5);
	    			if(pointscounter == pointslimit){	//Level Up
    					pointscounter=0;
    					progressbar.setProgress(pointscounter);
    					pointsView.setText(originalPoints + "\n" + plusPoints + "\n" + bonustimeleft + "\n" + points);
    					if(gamemode.equals("full") || gamemode.equals("quick")){
    						lvl++;
    						prefs.edit().putLong("userlvl", lvl).commit();
    		       			levelView.setText("Level: " + lvl);
    		       			pointslimit = (int) (lvl*10 + 100);
    		       			progressbar.setMax(pointslimit);
	    					toNextLevelView.setText(pointscounter + "/" + pointslimit);
	    					//Toast message to inform user of level up
	    					levelUpToast.show();
	    					if(sound){
	    			    		soundPool.play(Main.SNDlevelup, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    			    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    			    	}
    		       		}	  
    				}else if(points==newTotal){		//progress bar animation is finished
	    				progressbar.setProgress(pointscounter);
	    				updatepoints.removeCallbacksAndMessages(null);
	    				pointsView.setText(originalPoints + "\n" + plusPoints + "\n" + bonustimeleft + "\n" + points);
	    				toNextLevelView.setText(pointscounter + "/" + pointslimit);
	    				button1.setEnabled(true); button1.setBackgroundResource(R.drawable.paperbtnpressed); button1.setPadding(1, 1, 1, 1);
	    	       		button2.setEnabled(true); button2.setBackgroundResource(R.drawable.paperbtnpressed); button2.setPadding(1, 1, 1, 1);
	    	       		//Save to shared preferences
    		       		if(gamemode.equals("full") || gamemode.equals("quick")){
    		       			prefs.edit().putLong("pointscounter", pointscounter).commit();
    		       		}
    		       		if(sound){
    			    		soundPool.play(Main.SNDpop, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    			    	}
    				}else{
	    				progressbar.setProgress(pointscounter);
	    				pointsView.setText(originalPoints + "\n" + plusPoints + "\n" + bonustimeleft + "\n" + (points += 1));
	    				pointscounter++;
	    				toNextLevelView.setText(pointscounter + "/" + pointslimit);
	    			}
	    		}
	    	}, 500);
	    }
	}
	
	public void actnumsubmenubutton(View view){
		if(sound){
    		soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish(); 
	            	if(sound){
	    	    		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    	    	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	public void actnumsubagainbutton(View view){
		finish();	
		if(sound){
    		soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
		Intent intent=null;
		if(gamemode.equals("practice")){
			intent = new Intent(this, numbersSelect.class);
			points = 0;
		}else if(gamemode.equals("quick")){
			intent = new Intent(this, conundrum.class);
		}else {
			if(gameround == 5 || gameround == 10){
				intent = new Intent(this, ProgressScreen.class);				
			}else if(gameround == 14){
				intent = new Intent(this, conundrum.class);
			}
		}
		
    	intent.putExtra("gamemode", gamemode);
    	intent.putExtra("gameround", gameround);
    	intent.putExtra("points", points);
    	startActivity(intent);
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
	}
	
	private String defineusertext(){
		String a = null;
		int offset;
		if(useranswer > targetnum){
			offset = useranswer - targetnum;
			a = useranswer + " is " + offset + " over " + targetnum + ".";
		}else if(useranswer < targetnum){
			offset = targetnum - useranswer;
			a = useranswer + " is " + offset + " under " + targetnum + ".";
		}else{
			offset = 0;
			a = "Success! You got " + targetnum + ".";
		}
		
		if(offset < 10){
			a += " (+" + (10 - offset)*10 + " Points)";
		}
		
		return a;	
	}
	
	private int getpoints(){
		pointsnew = 0;
		
		int offset;
		if(useranswer == targetnum){
			pointsnew = 100;
		}else if(useranswer > targetnum){
			offset = useranswer - targetnum;
			if(offset < 10){
				pointsnew = (10 - offset)*10;
			}
		}else{
			offset = targetnum - useranswer;
			if(offset < 10){
				pointsnew = (10 - offset)*10;
			}
		}
		
		return pointsnew + bonustimeleft;		
	}
	
 	public void onBackPressed() {
		//make this an undo button
	    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:{
	            	finish(); 
	            	if(sound){
	    	    		soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
	    	    			audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    	    	}
	            }
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	                return;
	            }
	        }
	    };

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Return to Menu?").setPositiveButton("Yes", dialogClickListener)
	    .setNegativeButton("No", dialogClickListener).show();
	}
	
	//------------------------------------------------------------------------------------------------
	private static class Blob {
        int op;                 // Operator
        Blob left, right;       // Left and right operands
        int val;                // Value of expression
        int used;               // Bitmap of inputs used
        Blob next;              // Next blob with same inputs
        Blob hlink;             // Next blob in same hash chain
    }
	
	private static final char sym[] = { '?', '+', '-', '�', '�' };
	private static final int pri[] = { 0, 1, 1, 2, 2 };
	private static final int rpri[] = { 0, 1, 2, 2, 3 };
	
	/** Pretty-print an expression into a StringBuffer */
	private static void Walk(Blob e, int p, StringBuffer buf) {
	        if (e.op == CONST)
	            buf.append(e.val);
	        else {
	            int xp = pri[e.op], rp = rpri[e.op];
	            if (xp < p) buf.append('(');
	            Walk(e.left, xp, buf);
	            buf.append(" " + sym[e.op] + " ");
	            Walk(e.right, rp, buf);
	            if (xp < p) buf.append(')');
	        }
	    }
	    
	/** Convert an expression to a string for display */
	private static String Grind(Blob e) {
	        StringBuffer buf = new StringBuffer();
	        Walk(e, 1, buf);
	        return buf.toString();
	}
	   
	private static Blob pool[] = new Blob[expN];
	    
	private static Blob htable[] = new Blob[HSIZE];
	    
	private static int target;
	private static String best;
	private static int bestval, bestdist;

	/** Try to create a new blob with specified operator and arguments */
	private static void Add(int op, Blob p, Blob q, int val, int used) {
	        /* Return immediately if the expression is useless */
	        for (Blob r = htable[val % HSIZE]; r != null; r = r.hlink) {
	            if (r.val == val && (r.used & ~used) == 0)
	                return;
	        }
	        
	        /* Create the expression and add it to |pool| and |htable| */
	        Blob t = new Blob();
	        t.op = op; t.left = p; t.right = q; t.val = val; t.used = used;
	        t.next = pool[used]; pool[used] = t;
	        t.hlink = htable[val % HSIZE]; htable[val % HSIZE] = t;
	        
	        /* See if the new expression comes near the target */
	        int dist = Math.abs(val - target);
	        if (dist <= bestdist) {
	            String temp = Grind(t);
	            if (dist < bestdist || temp.length() < best.length()) {
	                bestval = val; bestdist = dist; best = temp;
	            }
	        }
	}                 
	    
	private static void Combine(int r, int s) {
	        int used = r | s;
	        for (Blob p = pool[r]; p != null; p = p.next) {
	            for (Blob q = pool[s]; q != null; q = q.next) {
	                if (p.val >= q.val) {
	                    Add(PLUS, p, q, p.val+q.val, used);
	                    if (p.val > q.val) Add(MINUS, p, q, p.val-q.val, used);
	                    Add(TIMES, p, q, p.val*q.val, used);
	                    if (q.val > 0 && p.val%q.val == 0)
	                        Add(DIVIDE, p, q, p.val/q.val, used);
	                }
	            }
	        }
	}
	    
	/* Set up a table of bitcounts in |ones| */
	private static int ones[] = new int[expN];
	    
	static {
	        // This uses the recurrence ones[i+2^n] = ones[i] + 1
	        ones[0] = 0;
	        for (int i = 0; i < N; i++) {
	            int t = 1 << i;
	            for (int r = 0; r < t; r++) ones[t+r] = ones[r]+1;
	        }
	}

	public static String Solve(ArrayList<Integer> mynumbers, int target) {
	        numbersSubmit.target = target;
	        bestdist = 1000000;

	        /* Empty the hash table and pools */
	        for (int i = 0; i < HSIZE; i++) htable[i] = null;
	        for (int r = 0; r < expN; r++) pool[r] = null;
	        
	        /* Plant the draw numbers as seeds */
	        for (int i = 0; i < N; i++)
	            Add(CONST, null, null, mynumbers.get(i), 1 << i);
	        
	        /* Combine using up to N-1 operations */
	        for (int i = 2; i <= N; i++) {
	            /* Combine disjoint pools that together use |i| inputs */
	            for (int r = 1; r < expN; r++) {
	                for (int s = 1; s < expN; s++) {
	                    if (ones[r] + ones[s] == i && (r & s) == 0)
	                        Combine(r, s);
	                }
	            }
	        }

	        if (bestdist == 0)
	            return "Solution: \n\n" + best + "\n\n= " + target;
	        else
	            return "Closest Solution: \n\n" + best + "\n\n= " + bestval;
	}
	
}
