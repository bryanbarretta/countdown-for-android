package com.bryan.barrett.countdownforandroid;

import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.ProgressTable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class playSubscreen extends Activity {
	
	ProgressTable progress;
	boolean gameinmemory, sound;
	Button continuebutton;
	SoundPool soundPool;
	AudioManager audio;
	SharedPreferences prefs;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_subscreen);
	    
	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;

	    TextView title = (TextView)findViewById(R.id.subtitletext); title.setTypeface(Main.scribble); title.setTextSize(Main.fontSize+13);
	    Button full = (Button)findViewById(R.id.fullbutton); full.setTypeface(Main.scribble); full.setTextSize(Main.fontSize+8);
	    Button quick = (Button)findViewById(R.id.quickbutton); quick.setTypeface(Main.scribble); quick.setTextSize(Main.fontSize+8);
	    Button pract = (Button)findViewById(R.id.practicebutton); pract.setTypeface(Main.scribble); pract.setTextSize(Main.fontSize+8);
	    continuebutton = (Button)findViewById(R.id.continuebutton); continuebutton.setTypeface(Main.scribble); continuebutton.setTextSize(Main.fontSize+8);
	}
	
	public void openFullGame(View view){
	    if(gameinmemory == true){	//if user hits new game and there is a game in memory...
	    	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		            switch (which){
		            case DialogInterface.BUTTON_POSITIVE:{	
		            	newFullGame();	//'Proceed' will start newFullGame()
		            }
		            case DialogInterface.BUTTON_NEGATIVE:
		                return;			//'Cancel' will do nothing
		            }
		        }
		    };
		    AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage("Warning: Starting a new game will overwrite your current game")
		    .setPositiveButton("Proceed", dialogClickListener).setNegativeButton("Cancel", dialogClickListener).show();
	    }else{						//if user hits new game and there is not a game in memory
	    	newFullGame();			//start newFullGame()
	    }
	    if(sound){
	    	soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	
	public void newFullGame(){
		//db
	    progress = new ProgressTable(this);
	    Intent intent = new Intent(this, lettersSelect.class);
    	intent.putExtra("gamemode", "full");
    	intent.putExtra("points", 0);
    	intent.putExtra("gameround", 0);
    	progress.clear();
	    progress.close();
    	startActivity(intent);
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
	}
	
	public void openQuickGame(View view){
    	Intent intent = new Intent(this, lettersSelect.class);
    	intent.putExtra("gamemode", "quick");
    	intent.putExtra("points", 0);
    	startActivity(intent);
    	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	if(sound){
    		soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
	
	public void continueGame(View view){
		//db
	    progress = new ProgressTable(this);
	    
	    /*Find out the points tally. Points are stored in string format in progress table seperated by \n 
	     *Also finds the gameround*/
	    String[] parts;
	    int results[];
	    int gameround = 0;
	    parts = progress.getAllPoints().split("\n");
	    progress.close();
	    results = new int[parts.length];
	    for (gameround = 0; gameround < parts.length; ++gameround) {
	        results[gameround] = Integer.parseInt(parts[gameround].trim());
	    }
	    int points = 0;
	    for (int i = 0; i < results.length; i ++){
	    	points = points + results[i];
	    }
	    
	    Intent intent = new Intent(this, ProgressScreen.class);
	    intent.putExtra("gamemode", "full");
	    intent.putExtra("gameround", gameround);
	    intent.putExtra("points", points);
	    startActivity(intent);
	    finish();
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	if(sound){
    		soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
	}
	
	public void openPracticeSubscreen(View view){
    	Intent intent = new Intent(this, practiceSubscreen.class);
    	startActivity(intent);
    	if(sound){
    		soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
	
	public void onResume(){
		super.onResume();
		progress = new ProgressTable(this);
	    if(progress.getAllRounds().equals("")){
	    	gameinmemory = false;
	    }else{
	    	gameinmemory = true;
	    }
	    
	    if(gameinmemory==false){
	    	continuebutton.setEnabled(false); continuebutton.setBackgroundResource(R.drawable.paperbtngreyed);
	    	continuebutton.setPadding(1, 1, 1, 1);
	    }else{
	    	continuebutton.setEnabled(true); continuebutton.setBackgroundResource(R.drawable.paperbtnpressed);
	    	continuebutton.setPadding(1, 1, 1, 1);
	    }
	    progress.close();
	}
	
	public void onBackPressed() {
	    super.onBackPressed();
	    if(sound){
	    	soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    }
	}

}
