package com.bryan.barrett.countdownforandroid;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import com.bryan.barrett.countdownforandroid.R;

public class practiceSubscreen extends Activity {

	SoundPool soundPool;
	AudioManager audio;
	SharedPreferences prefs;
	boolean sound;
	View view;
	Intent iNum, iLet, iCon;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_practice);

	    //Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    sound = prefs.getBoolean("sound", true);
	    
	    //Sounds
	    setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    soundPool = Main.soundPool;
	    
    	//iNum = new Intent(this, numbersSelect.class);
    	iLet = new Intent(this, lettersSelect.class);
    	iCon = new Intent(this, conundrum.class);
    	
	    TextView title = (TextView)findViewById(R.id.practicescreentitle); title.setTypeface(Main.scribble); 
	    title.setTextSize(Main.fontSize+13);
	    
	    Button letters = (Button)findViewById(R.id.practicescreenletbtn); letters.setTypeface(Main.scribble); 
	    	letters.setHeight(Main.screenh/4); letters.setTextSize(Main.fontSize+8);
	    Button numbers = (Button)findViewById(R.id.practicescreennumbtn); numbers.setTypeface(Main.scribble); 
	    	numbers.setHeight(Main.screenh/4); numbers.setTextSize(Main.fontSize+8);
	    Button conun = (Button)findViewById(R.id.practicescreenconbtn); conun.setTypeface(Main.scribble); 
	    	conun.setHeight(Main.screenh/4);   conun.setTextSize(Main.fontSize+8);
	}

	public void openNumbersSelect(View view){
		Intent iNum = new Intent(this, numbersSelect.class);
    	String gamemode = "practice";	//practice mode
    	iNum.putExtra("gamemode", gamemode);
    	startActivity(iNum);
    	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	if(sound){
	    	soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
	
	public void openLettersSelect(View view){
    	String gamemode = "practice";	//practice mode
    	
    	iLet.putExtra("gamemode", gamemode);
    	startActivity(iLet);
    	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	if(sound){
	    	soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
	
	public void openConunSelect(View view){
    	String gamemode = "practice";	//practice mode
    	iCon.putExtra("gamemode", gamemode);
    	startActivity(iCon);
    	
    	overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    	if(sound == true){
	    	soundPool.play(Main.SNDclick, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
    	}
    } 
	
	public void onBackPressed() {
	    super.onBackPressed();
	    if(sound){
	    	soundPool.play(Main.SNDback, audio.getStreamVolume(AudioManager.STREAM_MUSIC), 
					audio.getStreamVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
	    }
	}

}
