package com.bryan.barrett.countdownforandroid.viewpager;

import com.bryan.barrett.countdownforandroid.Main;
import com.bryan.barrett.countdownforandroid.R;
import com.bryan.barrett.countdownforandroid.database.HighScoresTable;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ScoreLayoutOne extends Fragment {
	
	static String Tpoints, Tnames, Ttimes;
	int cursorPos;
	static TextView cursor;
	Typeface scribble;
	int fontSize;
	SharedPreferences prefs;
	
	public static Fragment newInstance(Context context) {
		ScoreLayoutOne f = new ScoreLayoutOne();
		return f;
	}
	
	public static void setTable1(String p, String n, String t){
		Tpoints = p; Tnames = n; Ttimes = t;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	   
		//Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(Main.mainContext);
	    fontSize = prefs.getInt("fontSize", 27);

	    //fonts
	    Typeface scribble= Typeface.createFromAsset(Main.mainAssets, "fonts/scribble.ttf");
	    
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.score_layout_one, null);
		TextView points = (TextView)root.findViewById(R.id.FullScoreMenuQuickPoints);
		TextView names = (TextView)root.findViewById(R.id.FullScoreMenuQuickNames); 
		TextView times = (TextView)root.findViewById(R.id.FullScoreMenuQuickTimes); 
		cursor = (TextView)root.findViewById(R.id.FullScoreMenuCursor); 
		
		Button b = (Button)root.findViewById(R.id.FullScoreViewButton); b.setTypeface(scribble); b.setTextSize(fontSize-5);
		
		TextView title = (TextView)root.findViewById(R.id.FullScoreMenuQuickTitle); title.setTypeface(scribble);
			title.setTextSize(fontSize+8);
		points.setText("POINTS:\n\n" + Tpoints);
		names.setText( "NAME:\n\n" + Tnames);
		times.setText( "TIME & DATE:\n\n" + Ttimes);
		cursor.setText("\n\n <<");
		
		return root;
	}
	
	public static void moveCursor(int pos){
		if(pos == 0) 	  cursor.setText("\n\n <<");
		else if(pos == 1) cursor.setText("\n\n\n <<");
		else if(pos == 2) cursor.setText("\n\n\n\n <<");
		else if(pos == 3) cursor.setText("\n\n\n\n\n <<");
		else if(pos == 4) cursor.setText("\n\n\n\n\n\n <<");
		else if(pos == 5) cursor.setText("\n\n\n\n\n\n\n <<");
		else if(pos == 6) cursor.setText("\n\n\n\n\n\n\n\n <<");
		else if(pos == 7) cursor.setText("\n\n\n\n\n\n\n\n\n <<");
		else if(pos == 8) cursor.setText("\n\n\n\n\n\n\n\n\n\n <<");
		else if(pos == 9) cursor.setText("\n\n\n\n\n\n\n\n\n\n\n <<");
	}
	
}
