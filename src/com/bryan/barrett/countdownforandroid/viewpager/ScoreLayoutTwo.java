package com.bryan.barrett.countdownforandroid.viewpager;

import com.bryan.barrett.countdownforandroid.Main;
import com.bryan.barrett.countdownforandroid.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ScoreLayoutTwo extends Fragment {

	static String Tpoints, Tnames, Ttimes, Tcursor;
	static Typeface scribble;
	int fontSize;
	SharedPreferences prefs;
	
	public static Fragment newInstance(Context context) {
		ScoreLayoutTwo f = new ScoreLayoutTwo();	
		
		return f;
	}

	public static void setTable2(String p, String n, String t){
		Tpoints = p; Tnames = n; Ttimes = t;
	}
	
	public static void setFont(Typeface t){
		scribble = t;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		//Shared Preferences
	    prefs = PreferenceManager.getDefaultSharedPreferences(Main.mainContext);
	    fontSize = prefs.getInt("fontSize", 27);
	    
	    //fonts
	    Typeface scribble= Typeface.createFromAsset(Main.mainAssets, "fonts/scribble.ttf");
	    
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.score_layout_two, null);	
		TextView points = (TextView)root.findViewById(R.id.QuickScoreMenuQuickPoints); 
		TextView names = (TextView)root.findViewById(R.id.QuickScoreMenuQuickNames); 
		TextView times = (TextView)root.findViewById(R.id.QuickScoreMenuQuickTimes); 
		TextView title = (TextView)root.findViewById(R.id.QuickScoreMenuQuickTitle); title.setTypeface(scribble);
			title.setTextSize(fontSize+8);
		
		names.setText("POINTS:\n\n" + Tpoints);
		points.setText( "NAME:\n\n" + Tnames);
		times.setText( "TIME & DATE:\n\n" + Ttimes);
		
		return root;
	}
	
}