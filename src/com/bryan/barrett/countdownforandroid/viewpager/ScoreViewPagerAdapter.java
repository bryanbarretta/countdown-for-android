package com.bryan.barrett.countdownforandroid.viewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ScoreViewPagerAdapter extends FragmentPagerAdapter {
	private Context _context;
	
	public ScoreViewPagerAdapter(Context context, FragmentManager fm) {
		super(fm);	
		_context=context;		
		}
	@Override
	public Fragment getItem(int position) {
		Fragment f = new Fragment();
		switch(position){
		case 0:
			f=ScoreLayoutOne.newInstance(_context);	
			break;
		case 1:
			f=ScoreLayoutTwo.newInstance(_context);	
			break;
		}
		return f;
	}
	@Override
	public int getCount() {
		return 2;
	}

}
